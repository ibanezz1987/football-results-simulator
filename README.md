# Football results simulator

### Live demo:

[competitiondesigner.frankvanboxtel.com](https://competitiondesigner.frankvanboxtel.com)

### Todo list:

- Add intro text:
  Football clubs and national teams are exiting for fans and everybody involved in the sport. A team that doen't play doesn't count. So the teams get this attention from exciting competitions they play in. Competitions are hot.
  As a result we see a lot of initiatives to introduce, change and scale up competitions. Associations and clubs are growing more aware of the possibilities.
  FIFA is introducing 2 year World Cups and an expanded Club World Cup. UEFA has expanded its EURO competition to 24 nations, introduces the Nations League and drastically revised the Champions League from season 2024–25 onwards. Meanwhile 12 clubs announced the controversial Super League.
  These competitions structure however, do vary a lot from each other. So what is the ideal competition format for a situation? Can we do better? That is what I try to answer based on simulations made in this site. Simulations created by me & you.

##### Cups

- Add layers of stages.
  - Add specific promotion/relegation rules between layers of stages

##### Visual

- Add sorting of knockout phases after it has completed
- Add stage explainations (also show how teams qualified if thats the case)
- Add ability to view match results
- Add ability to view team results
- Add simulation mode with different speeds to make the app feel 'alive'
- Add option to simulate multiple years

##### Stage draws

- Allow super cup (even when one team wins both league and cup)
- Setting to prevent teams drawing (random/performance/reputation) from same group as the previous group as much as possible.
  - Also a setting to stimulate same group draws
- Setting to prevent + Setting to encourage same nations draws as much as possible

### Lower priority

##### Sorting

- Add configurable sorting rules
- Add custom points for results 3 value array (win, draw, loss)
- Add custom points for results 7 value array (3 goal win, 2 gw, 1 gw, draw, 1 gl, 2 gl, 3 gl)
- Add points and max points for goals in a match - Also option to subtract points for goals against

##### Teams

- Generate missing teams like 'Bel FC #1'

##### Variable stage length

- Add option to finish competition when a team can't be overtaken (like best of 5 final in basketball)
- Optional stage if there multiple winners (like a aperture/clausura final)
  - Or: Qualify rules for next cups (less complexity)

##### Custom competitions

- Add a international 'Ladder' competitie. 2vs3 4vs5, 1vs2, 3vs4, 2vs3 4vs5 etc
- Make a single stage KO phase with safety net possible (1 loss = ok, team that wins all games have an extra chance)

##### Simulation

- Add home/away advantage (after home/away draw is made equal) as a setting
- Make sure teams play mostly the same amount of games away as at home

### Done

- ~~Fixed groups, groups with given set of teams (Copa America 2021, North Zone, South Zone)~~
  - ~~Accept wider selection of teams and then only choose the qualified onces (if this is possible)~~
- ~~Add penalties/goals after extra time, maybe simulate these always but only use them when needed (see also above)~~
- ~~Add weighted draw based on coefficients~~
- ~~Add weighted draw based on previous stage performance~~
- ~~Add third place playoff (Olympics, Nations League, World Cup)~~
- ~~Take all received points or a part of them to the next stage (like 0.5x in the Belgium league)~~
- ~~option to add only factor of last rounds points~~
- ~~split the bottom into a new league (possible with the new sorting for the 3th place final)~~
- ~~Limiter on league matches (like the 10 in the new Champions League)~~
- ~~Splitted Leagues (Major League, West, Central East)~~
- ~~Add all countries~~
- ~~Allow teams to be added in a later stage~~
- ~~Add inbetween round - (like in the new Champions League, 9-24 play a round, winners play vs 1-8)~~
  - ~~Or: set bye for teams from-to: bye: [1, 8]~~
- ~~Add a variant of Drawtype.EqualPerformance: one that keep higher league results separate from lower league results (also allows a safety net KO match like in the A-League playoffs)~~
  - ~~Or a smart mechanism that detects that previous round is based on "EqualPerformance" > then keep these results higher in sorting~~
- ~~Take all received points in matches between advancing teams to the next stage~~
