import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import {
  exampleCompetitions,
  exampleLeagueSystems,
} from 'src/app/data/example-competitions';
import { iCompetitionSettings, iLeagueSystem } from 'src/app/models';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  leagueSystems: iLeagueSystem[];
  competitions: iCompetitionSettings[];
  formGroup = this.formBuilder.group({
    selectedCompetition: [100001],
    selectedLeagueSystem: [1000],
  });

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    protected readonly router: Router
  ) {}

  ngOnInit() {
    this.getCompetitions();
    this.getLeagueSystems();

    this.activatedRoute.params.pipe(take(1)).subscribe((params) => {
      const selectedCompetitionId = params?.competitionId;
      if (selectedCompetitionId) {
        this.formGroup
          .get('selectedCompetition')
          .setValue(selectedCompetitionId);
      }
    });
  }

  openCompetition() {
    const selectedCompetitionId = this.formGroup.get(
      'selectedCompetition'
    ).value;
    if (selectedCompetitionId) {
      const competitionSettings = exampleCompetitions.find(
        (competition) => competition.id === Number(selectedCompetitionId)
      );
      this.router.navigate([
        '/competition',
        selectedCompetitionId,
        competitionSettings?.name.replace(/ +/g, ''),
      ]);
    }
  }

  openLeagueSystem() {
    const selectedLeagueSystemId = this.formGroup.get(
      'selectedLeagueSystem'
    ).value;
    if (selectedLeagueSystemId) {
      const leagueSystem = exampleLeagueSystems.find(
        (system) => system.id === Number(selectedLeagueSystemId)
      );
      this.router.navigate([
        '/leaguesystem',
        selectedLeagueSystemId,
        leagueSystem?.name.replace(/ +/g, ''),
      ]);
    }
  }

  createCompetition() {
    alert('Work in progress');
  }

  private getCompetitions() {
    this.competitions = exampleCompetitions;
  }

  private getLeagueSystems() {
    this.leagueSystems = exampleLeagueSystems;
  }
}
