import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { iCompetitionSettings, iCompetitionStageData } from 'src/app/models';
import { take, takeUntil } from 'rxjs/operators';
import {
  exampleCompetitions,
  exampleLeagueSystems,
} from 'src/app/data/example-competitions';
import { LeagueSystem } from 'src/app/class/league-system';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.scss'],
})
export class CompetitionComponent implements OnInit, OnDestroy {
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  leagueSystem: LeagueSystem;
  competitionSettings: iCompetitionSettings;
  competitionData: iCompetitionStageData[];
  zoomLevel = 1;
  title = '';
  nthCompetition: number;
  isSingleCompetition = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    protected readonly router: Router
  ) {}

  ngOnInit() {
    this.activatedRoute.params.pipe(take(1)).subscribe((params) => {
      if (params.leagueSystemId) {
        this.loadLeagueSystem(Number(params.leagueSystemId));
      }
      if (params.competitionId) {
        this.loadCompetition(Number(params.competitionId));
      }
    });

    this.activatedRoute.queryParams
      .pipe(takeUntil(this.destroyed$))
      .subscribe((queryParams) => {
        this.nthCompetition =
          queryParams && queryParams.c ? Number(queryParams.c) : 0;
        this.updateActiveCompetition();
      });

    this.title = this.leagueSystem?.getTitle();
    this.isSingleCompetition = this.leagueSystem.isSingleCompetition();

    console.log('');
    console.log('CompetitionComponent leagueSystem', this.leagueSystem);
    console.log('this.competitionData', this.competitionData);
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  changeActiveCompetition(competitionIndex: number) {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {
        c: competitionIndex,
      },
      queryParamsHandling: 'merge',
    });
  }

  updateActiveCompetition() {
    this.competitionSettings = this.leagueSystem.getCompetitionSettings(
      this.nthCompetition
    );
    this.competitionData = this.leagueSystem.getCompetitionData(
      this.nthCompetition
    );
  }

  home() {
    this.router.navigate(['', this.competitionSettings?.id]);
  }

  zoomIn() {
    this.zoomLevel = this.zoomLevel + 0.25;
  }

  zoomOut() {
    const newZoomLevel = this.zoomLevel - 0.25;
    this.zoomLevel = newZoomLevel > 0 ? newZoomLevel : 0.25;
  }

  private loadLeagueSystem(leagueSystemId: number) {
    const leagueSystemSettings = this.findLeagueSystem(leagueSystemId);
    this.leagueSystem = !leagueSystemSettings
      ? null
      : new LeagueSystem(leagueSystemSettings);
  }

  private findLeagueSystem(leagueSystemId: number) {
    return exampleLeagueSystems.find((system) => system.id === leagueSystemId);
  }

  private loadCompetition(competitionId: number) {
    const competitionSettings = this.findCompetition(competitionId);
    this.leagueSystem = new LeagueSystem({
      id: -1,
      name: competitionSettings?.name,
      competitions: [competitionSettings],
    });
  }

  private findCompetition(competitionId: number) {
    return exampleCompetitions.find(
      (competition) => competition.id === competitionId
    );
  }
}
