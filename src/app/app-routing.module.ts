import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompetitionComponent } from './page/competition/competition.component';
import { HomeComponent } from './page/home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: ':competitionId',
    component: HomeComponent,
  },
  {
    path: 'competition/:competitionId/:competitionName',
    component: CompetitionComponent,
  },
  {
    path: 'leaguesystem/:leagueSystemId/:leagueSystemName',
    component: CompetitionComponent,
  },
  {
    path: 'leaguesystem/:leagueSystemId/:leagueSystemName',
    component: CompetitionComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
