import { teamsList } from '../data/teams';
import {
  DrawType,
  iCompetitionStageData,
  iCompetitionStageSettings,
  iStageSettings,
  iTeam,
} from '../models';
import { Sort } from './sort';
import { Stage } from './stage';
import { Standing } from './standing';

export class Competition {
  private readonly sort = new Sort();
  private competitionStages: iCompetitionStageData[] = [];

  constructor(readonly competitionStagesSettings: iCompetitionStageSettings[]) {
    const teamIds = competitionStagesSettings[0].stageSettings.teamIds;
    let stageTeams = this.getTeams(teamIds);
    competitionStagesSettings[0].stageSettings.teams = stageTeams;

    let combinedGroupStandings: Standing[] = [];
    let previousCombinedGroupStandings: Standing[][] = [];
    this.competitionStages = competitionStagesSettings;

    // Walk through all stages of the competition
    this.competitionStages.forEach(
      (
        {
          stageSettings,
          teamsEntering = Infinity,
          teamsSkipping = 0,
          totalGroups = 1,
          drawType,
        },
        stageIndex
      ) => {
        const competitionStage = this.competitionStages[stageIndex];
        // Get active teams
        const includedTeams = this.getIncludedTeams(
          competitionStage,
          stageTeams,
          teamsEntering,
          teamsSkipping
        );
        // Get teams as a sorted list
        const orderedTeams = this.getDrawOrder(
          drawType,
          includedTeams,
          combinedGroupStandings
        );
        // Get groups
        const groupedTeams = this.getGroupedTeams(
          drawType,
          orderedTeams,
          totalGroups
        );
        // Initialize stage params
        combinedGroupStandings = [];
        competitionStage.standings = [];
        competitionStage.schedule = [];
        competitionStage.skippingTeams = stageTeams.slice(0, teamsSkipping);

        // Walk through each set of grouped teams (groups or ko)
        groupedTeams.forEach((teams) => {
          const settings: iStageSettings = {
            ...stageSettings,
            teams,
          };
          const previousStandings =
            previousCombinedGroupStandings[stageIndex - 1] ?? [];
          const stage = new Stage(
            competitionStage,
            settings,
            previousStandings
          );

          // Standings
          const standings = stage.getStandings();
          const updatedCombinedStandings = [
            ...combinedGroupStandings,
            ...standings,
          ];
          if (drawType === DrawType.EqualPerformance) {
            combinedGroupStandings = updatedCombinedStandings;
          } else {
            combinedGroupStandings = new Sort().standings(
              updatedCombinedStandings
            );
          }
          competitionStage.standings.push(standings);

          // Schedule
          competitionStage.schedule.push(stage.getSchedule());

          // Subleagues
          const subLeagueStandings = stage.getSubLeagueStandings();
          competitionStage.standings = [
            ...competitionStage.standings,
            ...subLeagueStandings,
          ];
          if (subLeagueStandings.length) {
            let combinedStandings = [];
            subLeagueStandings.forEach((standings) => {
              combinedStandings = [...combinedStandings, ...standings];
            });
            combinedGroupStandings = new Sort().standings([
              ...combinedStandings,
            ]);
          }
        });

        // Save stage teams as an sorted list (of all teams that participated in stage)
        stageTeams = [
          ...competitionStage.skippingTeams,
          ...combinedGroupStandings.map((standing) => standing.team),
        ];

        // Save standings as an sorted list (with all teams that participated in stage)
        combinedGroupStandings = [
          ...competitionStage.skippingTeams.map((team) =>
            new Standing(team).setPosition(0)
          ),
          ...combinedGroupStandings,
        ];
        previousCombinedGroupStandings[stageIndex] = combinedGroupStandings;
        competitionStage.combinedGroupStandings = combinedGroupStandings;
      }
    );

    // Set advancing flag for display purposes
    this.setAdvancingTeamsValue(previousCombinedGroupStandings);
  }

  getCompetitionData() {
    return this.competitionStages;
  }

  private setAdvancingTeamsValue(previousCombinedGroupStandings: Standing[][]) {
    let advancingTeamsList: number[] = [];

    for (let i = previousCombinedGroupStandings.length - 1; i >= 0; i--) {
      const stageStandings = previousCombinedGroupStandings[i];
      stageStandings.forEach((standing) => {
        const teamId = standing.team.id;
        const isTeamInList = advancingTeamsList.indexOf(teamId) !== -1;
        const isLastStage = previousCombinedGroupStandings.length - 1;
        if (isTeamInList || (isLastStage && standing.position < 2)) {
          standing.isAdvancing = true;
        }
        advancingTeamsList.push(teamId);
      });
    }
  }

  private getDrawOrder(
    drawType: DrawType,
    teams: iTeam[],
    combinedGroupStandings: Standing[]
  ) {
    switch (drawType) {
      case DrawType.Reputation:
        return this.sort.teamsOnReputation(teams);
      case DrawType.Performance:
      case DrawType.EqualPerformance:
        return this.sort.teamsOnPerformance(teams, combinedGroupStandings);
      default:
        return this.sort.teamsRandomized(teams);
    }
  }

  private getIncludedTeams(
    competitionStage: iCompetitionStageData,
    stageTeams: iTeam[],
    teamsEntering: number,
    teamsSkipping: number
  ) {
    if (competitionStage.stageSettings?.teams?.length) {
      stageTeams = competitionStage.stageSettings.teams;
    }
    return [...stageTeams.slice(teamsSkipping, teamsEntering + teamsSkipping)];
  }

  private getGroupedTeams(
    drawType: DrawType,
    teams: iTeam[],
    totalGroups: number
  ) {
    if (drawType === DrawType.EqualPerformance) {
      return this.getGroupsBasedOnStrength(teams, totalGroups);
    }
    return this.getDistributedGroups(teams, totalGroups);
  }

  private getGroupsBasedOnStrength(teams: iTeam[], totalGroups: number) {
    return this.getEqualSizedGroups(teams, totalGroups);
  }

  private getDistributedGroups(teams: iTeam[], totalGroups: number) {
    const groups: iTeam[][] = [];

    for (let i = 0; i < totalGroups; i++) {
      groups.push([]);
    }
    teams.forEach((team, index) => {
      const targetGroupIndex = index % totalGroups;
      groups[targetGroupIndex] = [...groups[targetGroupIndex], team];

      if (targetGroupIndex === totalGroups - 1) {
        groups.reverse();
      }
    });
    return groups;
  }

  private getEqualSizedGroups(a: iTeam[], n: number) {
    if (n < 2) {
      return [a];
    }
    const len = a.length;
    const out = [];
    let i = 0;
    let size;
    if (len % n === 0) {
      size = Math.floor(len / n);
      while (i < len) {
        out.push(a.slice(i, (i += size)));
      }
    } else {
      while (i < len) {
        size = Math.ceil((len - i) / n--);
        out.push(a.slice(i, (i += size)));
      }
    }
    return out;
  }

  private getTeams(teamIds: number[]) {
    const teams: iTeam[] = [];
    teamIds?.forEach((id) => {
      const team = teamsList.find((team) => team.id === id);
      if (team) {
        teams.push(team);
      }
    });
    return teams;
  }
}
