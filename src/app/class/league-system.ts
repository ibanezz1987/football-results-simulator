import {
  iCompetitionSettings,
  iCompetitionStageData,
  iLeagueSystem,
  iTeam,
} from '../models';
import { Competition } from './competition';

export class LeagueSystem {
  private competitions: Competition[] = [];
  private competitionSettingsArray: iCompetitionSettings[] = [];
  private competitionDataArray: iCompetitionStageData[][] = [];

  constructor(private leagueSystem: iLeagueSystem) {
    if (this.leagueSystem) {
      this.createCompetitions();
    }
  }

  getTitle() {
    return this.leagueSystem?.name;
  }

  getCompetitionSettings(index = 0) {
    return this.competitionSettingsArray[index];
  }

  getCompetitionData(index = 0) {
    return this.competitionDataArray[index];
  }

  isSingleCompetition() {
    return this.competitions.length === 1;
  }

  private createCompetitions() {
    this.leagueSystem.competitions.forEach(
      (competitionSettings, competitionIndex) => {
        this.applyIncludedTeams(competitionSettings, competitionIndex);

        const competition = new Competition(competitionSettings.stages);
        this.competitions.push(competition);
        this.competitionSettingsArray.push(competitionSettings);
        this.competitionDataArray.push(competition.getCompetitionData());
      }
    );
  }

  private applyIncludedTeams(
    competitionSettings: iCompetitionSettings,
    competitionIndex: number
  ) {
    const includedTeamsLines =
      this.leagueSystem.includedTeamsFromOtherCompetition;

    if (!includedTeamsLines) {
      return;
    }

    const includedTeamsData = includedTeamsLines.filter(
      (teams) => teams.targetIndex === competitionIndex
    );
    let includedTeamsArrays: iTeam[][] = [];

    if (includedTeamsData.length === 0) {
      return;
    }

    includedTeamsData.forEach((included) => {
      const competition = this.competitionDataArray[included.sourceIndex];
      const combinedGroupStandings =
        competition[competition.length - 1].combinedGroupStandings;
      const standings = combinedGroupStandings.slice(0, included.teamsEntering);
      includedTeamsArrays.push(standings.map((standing) => standing.team));
    });

    competitionSettings.stages[0].stageSettings.teamIds =
      this.getStaggeredArray(includedTeamsArrays).map((team) => team.id);
  }

  private getStaggeredArray(includedTeamsArrays: iTeam[][]) {
    let sortedTeamsArrays: iTeam[][] = [];
    includedTeamsArrays.forEach((teamsArray) => {
      teamsArray.forEach((team, teamIndex) => {
        if (!sortedTeamsArrays[teamIndex]) {
          sortedTeamsArrays[teamIndex] = [team];
        } else {
          sortedTeamsArrays[teamIndex] = [
            ...sortedTeamsArrays[teamIndex],
            team,
          ];
        }
      });
    });
    return [].concat(...sortedTeamsArrays) as iTeam[];
  }
}
