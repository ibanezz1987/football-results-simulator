import { MatchFases } from '../models';
import { Match } from './match';

export class Simulation {
  private penaltyConversionRate = 0.77;
  private mainPenaltySeriesLength = 5;

  constructor(private readonly schedule: Match[]) {
    this.getScheduleResults();
  }

  getResults() {
    return this.schedule;
  }

  private getScheduleResults() {
    this.schedule.forEach((match) => {
      this.getMatchResult(match);
    });
  }

  private getMatchResult(match: Match) {
    const expectedGoals = this.getExpectedGoals(match, 1);
    match.result = this.generateMatchResult(expectedGoals);

    if (match.tieBreakType === MatchFases.ExtraTime) {
      const expectedExtraTimeGoals = this.getExpectedGoals(match, 0.2);
      const extraTimeGoals = this.generateMatchResult(expectedExtraTimeGoals);
      match.extraTimeResult = [
        match.result[0] + extraTimeGoals[0],
        match.result[1] + extraTimeGoals[1],
      ];
    }

    if (
      match.tieBreakType === MatchFases.Penalties ||
      match.tieBreakType === MatchFases.ExtraTime
    ) {
      match.penaltyResult = this.simulatePenaltyResult(match);
    }
  }

  private generateMatchResult(expectedGoals: number[]) {
    const randomFactor = 6; // size of the distribution positive and negative
    const rawGoalsT1 = Math.round(
      this.getNormallyDistributedRandom() * randomFactor -
        randomFactor / 2 +
        expectedGoals[0]
    );
    const rawGoalsT2 = Math.round(
      this.getNormallyDistributedRandom() * randomFactor -
        randomFactor / 2 +
        expectedGoals[1]
    );
    const goalsT1 = rawGoalsT1 < 0 ? 0 : rawGoalsT1;
    const goalsT2 = rawGoalsT2 < 0 ? 0 : rawGoalsT2;
    return [goalsT1, goalsT2];
  }

  private getExpectedGoals(match: Match, goalFactor: number) {
    const expectedGoalsMinimum = goalFactor * 2.5; // Every match has a good chance of goals
    const expectedGoalsFactor = 40; // Lower is more goals
    const teams = match.teams;

    // Calculate strength of teams
    const absoluteT1 = teams[0].strength - teams[1].strength;
    const relativeT1 =
      (100 / (teams[0].strength + teams[1].strength)) * teams[0].strength;
    const relativeT2 =
      (100 / (teams[0].strength + teams[1].strength)) * teams[1].strength;

    // Calculate average goals
    const expectedGoalsTotal =
      Math.abs(absoluteT1) / expectedGoalsFactor + expectedGoalsMinimum;
    const expectedGoalsT1 = (expectedGoalsTotal * relativeT1) / 100;
    const expectedGoalsT2 = (expectedGoalsTotal * relativeT2) / 100;

    return [expectedGoalsT1, expectedGoalsT2];
  }

  private getNormallyDistributedRandom() {
    let u = 0,
      v = 0;
    while (u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while (v === 0) v = Math.random();
    let num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) return this.getNormallyDistributedRandom(); // resample between 0 and 1
    return num;
  }

  private simulatePenaltyResult(match: Match) {
    let currentPenalty = 0;
    let scores = [0, 0];
    while (
      currentPenalty < this.mainPenaltySeriesLength ||
      scores[0] === scores[1]
    ) {
      for (let i = 0; i < match.teams.length; i++) {
        if (this.simulatedPenaltyScored()) {
          scores[i]++;
        }
      }
      currentPenalty++;
    }
    return scores;
  }

  private simulatedPenaltyScored() {
    return Math.random() < this.penaltyConversionRate;
  }
}
