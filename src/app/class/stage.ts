import {
  iCompetitionStageData,
  iStageSettings,
  iTeam,
  MatchFases,
  MathRoundType,
  ScheduleType,
} from '../models';
import { Standing } from './standing';
import { Match } from './match';
import { Simulation } from './simulation';
import { Sort } from './sort';

export class Stage {
  private standings: Standing[] = [];
  private subLeagueStandings: Standing[][] = [];
  private schedule: Match[] = [];
  private defaultRounds = 2;

  constructor(
    private readonly competitionStage: iCompetitionStageData,
    private readonly settings: iStageSettings,
    private readonly previousCombinedGroupStandings: Standing[]
  ) {
    this.createInitialStandings();
    this.createLeagueSchedule();

    this.schedule = new Simulation(this.schedule).getResults();

    this.generateStandings();
    this.generateSubLeagueStandings();
    this.tieBrakeEvenStandings();

    this.standings = this.getCleanStandings();
  }

  getSchedule() {
    return this.schedule;
  }

  getStandings() {
    return this.standings;
  }

  getSubLeagueStandings() {
    return this.subLeagueStandings;
  }

  private createInitialStandings() {
    const teams = this.settings.teams ?? [];
    const previousRoundPointsFactor =
      this.competitionStage.previousRoundPointsFactor;
    const pointRoundingStrategy = this.competitionStage.pointRoundingStrategy;

    teams.forEach((team) => {
      const standing: Standing = new Standing(team);
      const previousStanding = this.previousCombinedGroupStandings.find(
        (standing) => standing.team === team
      );
      const rawPoints =
        !previousStanding || !previousRoundPointsFactor
          ? 0
          : previousRoundPointsFactor * previousStanding.points;
      const roundedPoints = this.getRoundedPoints(
        rawPoints,
        pointRoundingStrategy
      );
      this.standings[team.id] = standing.setInitialPoints(roundedPoints);
    });
  }

  private getRoundedPoints(rawPoints, pointRoundingStrategy) {
    switch (pointRoundingStrategy) {
      case MathRoundType.RawValue:
        return rawPoints;
      case MathRoundType.Round:
        return Math.round(rawPoints);
      case MathRoundType.RoundUp:
        return Math.ceil(rawPoints);
      case MathRoundType.RoundDown:
        return Math.floor(rawPoints);
      case MathRoundType.Round1Decimal:
        return Math.round(rawPoints * 10) / 10;
      case MathRoundType.Round2Decimals:
        return Math.round(rawPoints * 100) / 100;
      case MathRoundType.Round3Decimals:
        return Math.round(rawPoints * 1000) / 1000;
      default:
        return rawPoints;
    }
  }

  private createLeagueSchedule() {
    const teams = this.settings.teams ?? [];
    const rounds = this.competitionStage.rounds ?? this.defaultRounds;
    const totalMatchesPerRound = Math.ceil(teams.length / 2);
    let homeGroup: any[] = teams.slice(0, Math.ceil(teams.length / 2));
    let awayGroup: any[] = teams.slice(Math.ceil(teams.length / 2)).reverse();
    const isOdd = this.isOdd(teams.length);
    const matchDaysLimit = this.competitionStage.matchDaysLimit;
    let totalMatchDays = isOdd
      ? teams.length * rounds
      : (teams.length - 1) * rounds;
    totalMatchDays =
      matchDaysLimit && matchDaysLimit < totalMatchDays
        ? matchDaysLimit
        : totalMatchDays;

    if (isOdd) {
      awayGroup.push(false);
    }

    // Create rounds
    for (let i = 0; i < totalMatchDays; i++) {
      // Create matches
      for (let j = 0; j < totalMatchesPerRound; j++) {
        const homeTeam: iTeam = homeGroup[j];
        const awayTeam: iTeam = awayGroup[j];
        const onlyPlayCrossSubLeagues =
          this.settings.scheduleType === ScheduleType.CrossSubLeagues;
        const onlyPlayInterSubLeagues =
          this.settings.scheduleType === ScheduleType.InterSubLeagues;

        if (
          (onlyPlayCrossSubLeagues &&
            this.teamsAreFromSameSubLeague(homeTeam, awayTeam)) ||
          (onlyPlayInterSubLeagues &&
            !this.teamsAreFromSameSubLeague(homeTeam, awayTeam))
        ) {
          continue;
        }

        if (homeTeam && awayTeam) {
          const isLastMatchDay = i === totalMatchDays - 1;
          const tieBrakeType = isLastMatchDay
            ? this.competitionStage.tiebrakeBy
            : MatchFases.RegularTime;
          this.schedule.push(new Match(homeTeam, awayTeam, tieBrakeType));
        }
      }

      // Move last team from homeGroup to end of awayGroup
      awayGroup.push(homeGroup.pop());

      // Move first team from awayGroup to 2nd position of homeGroup
      homeGroup.splice(1, 0, awayGroup.shift());
    }
  }

  private generateStandings() {
    // Apply scores
    this.schedule.forEach((match: Match) => {
      if (match.result.length) {
        match.teams.forEach((team: iTeam, i: number) => {
          const standing: Standing = this.standings[team.id];
          const goalsScored: number = match.result[i];
          const goalsAgainst: number = this.getHighestOtherValue(
            match.result,
            goalsScored
          );
          const goalDifference: number = goalsScored - goalsAgainst;
          let points: number =
            goalDifference > 0 ? 3 : goalDifference === 0 ? 1 : 0;

          this.standings[team.id] = standing
            .addMatch()
            .addPoints(points)
            .addGoalDifference(goalDifference)
            .addGoalsScored(goalsScored)
            .addGoalsConceived(goalsAgainst);
        });
      }
    });
  }

  private tieBrakeEvenStandings() {
    const cleanStandings = this.standings.filter((standing) => !!standing);
    if (cleanStandings.length !== 2) {
      return;
    }
    const teamA = cleanStandings[0];
    const teamB = cleanStandings[1];

    if (
      teamA.points === teamB.points &&
      teamA.goalsScored - teamA.goalsConceived === 0
    ) {
      const lastMatch = this.schedule[this.schedule.length - 1];

      lastMatch.teams.forEach((team: iTeam, i: number) => {
        const standing: Standing = this.standings[team.id];
        let isDecided = false;

        if (lastMatch.tieBreakType === MatchFases.RegularTime) {
          isDecided = true;
        }

        // Extra time
        if (!isDecided && lastMatch.tieBreakType === MatchFases.ExtraTime) {
          const extraTimeScored = lastMatch.extraTimeResult[i];
          const extraTimeAgainst = this.getHighestOtherValue(
            lastMatch.extraTimeResult,
            extraTimeScored
          );
          const goalDifference = extraTimeScored - extraTimeAgainst;

          if (goalDifference !== 0) {
            // Decided during extra time
            lastMatch.decidedDuring = MatchFases.ExtraTime;
            isDecided = true;
          }
          if (goalDifference > 0) {
            standing.points++;
          }
        }

        // Penalties
        if (!isDecided && lastMatch.tieBreakType !== MatchFases.RegularTime) {
          const penaltiesScored: number = lastMatch.penaltyResult[i];
          const penaltiesAgainst: number = this.getHighestOtherValue(
            lastMatch.penaltyResult,
            penaltiesScored
          );
          const penaltyDifference: number = penaltiesScored - penaltiesAgainst;

          if (penaltyDifference > 0) {
            lastMatch.decidedDuring = MatchFases.Penalties;
            standing.points++;
            isDecided = true;
          }
        }
      });
    }
  }

  private getCleanStandings() {
    let standings: Standing[] = [];
    this.standings.forEach((originalStanding) => {
      standings.push(originalStanding.copy());
    });
    // Remove empty
    standings = standings.filter((el) => {
      return el != null;
    });
    // Sorting
    standings = new Sort().standings(standings);
    standings.forEach((standing, index) => {
      standing.setPosition(index + 1);
    });
    return standings;
  }

  private generateSubLeagueStandings() {
    // Copy standings from original league table
    const subLeagues = this.settings.subLeagues;
    if (!subLeagues) {
      return;
    }
    subLeagues.forEach((subLeague) => {
      const subLeagueStandings = [];
      const subLeagueTeams = [];
      subLeague.teamIds.forEach((teamId) => {
        const foundTeam = this.settings.teams.find(
          (team) => team.id === teamId
        );
        if (foundTeam) {
          subLeagueTeams.push(foundTeam);
        }
      });
      subLeagueTeams.forEach((team) => {
        const standings = this.standings.filter(
          (standing) => standing.team === team
        );
        if (standings.length) {
          const newStanding = new Standing(team)
            .addStanding(standings[0])
            .setPosition();
          subLeagueStandings.push(newStanding);
        }
      });
      const sortedStandings = new Sort().standings(subLeagueStandings);
      this.subLeagueStandings.push(sortedStandings);
    });
    // Update positions
    this.subLeagueStandings.forEach((subLeagueStandings) => {
      subLeagueStandings.forEach((standing, index) => {
        standing.setPosition(index + 1);
      });
    });
  }

  private teamsAreFromSameSubLeague(homeTeam: iTeam, awayTeam: iTeam) {
    const subLeagues = this.settings.subLeagues;
    for (let i = 0; i < subLeagues.length; i++) {
      const subLeague = subLeagues[i];
      const foundHomeTeam = subLeague.teamIds.find(
        (teamId) => teamId === homeTeam.id
      );
      const foundAwayTeam = subLeague.teamIds.find(
        (teamId) => teamId === awayTeam.id
      );
      if (foundHomeTeam && foundAwayTeam) {
        return true;
      }
    }
    return false;
  }

  private isOdd(n: number) {
    return Math.abs(n % 2) == 1;
  }

  private getHighestOtherValue(
    values: number[],
    ignoredValue?: number
  ): number {
    let highestValue: number = -Infinity;
    let foundIgnoreValue: boolean = false;

    values.forEach((value) => {
      if (!foundIgnoreValue && ignoredValue && ignoredValue === value) {
        foundIgnoreValue = true;
      } else {
        highestValue = value > highestValue ? value : highestValue;
      }
    });

    return highestValue;
  }
}
