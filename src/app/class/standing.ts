import { iTeam } from '../models';

export class Standing {
  team: iTeam;
  position: number;
  matches: number = 0;
  points: number = 0;
  wins: number = 0;
  draws: number = 0;
  losses: number = 0;
  goalDifference: number = 0;
  goalsScored: number = 0;
  goalsConceived: number = 0;
  isAdvancing: boolean = false;

  constructor(team: iTeam) {
    this.team = team;
    return this;
  }

  setPosition(position?: number) {
    this.position = position;
    return this;
  }

  addStanding(standing: Standing, pointsMultiplier = 1) {
    this.position = !this.position ? standing.position : null;
    this.matches += standing.matches;
    this.points += standing.points * pointsMultiplier;
    this.wins += standing.wins;
    this.draws += standing.draws;
    this.losses += standing.losses;
    this.goalsScored += standing.goalsScored;
    this.goalsConceived += standing.goalsConceived;
    this.goalDifference += standing.goalDifference;
    return this;
  }

  setStanding(standing: Standing) {
    this.position = standing.position;
    this.matches = standing.matches;
    this.points = standing.points;
    this.wins = standing.wins;
    this.draws = standing.draws;
    this.losses = standing.losses;
    this.goalsScored = standing.goalsScored;
    this.goalsConceived = standing.goalsConceived;
    this.goalDifference = standing.goalDifference;
    return this;
  }

  copy() {
    return new Standing(this.team).setStanding(this);
  }

  addMatch() {
    this.matches++;
    return this;
  }

  setInitialPoints(points: number) {
    this.points = points;
    return this;
  }

  addPoints(points: number) {
    this.points += points;
    switch (points) {
      case 3:
        this.addWins();
        break;
      case 0:
        this.addLosses();
        break;
      default:
        this.addDraws();
        break;
    }
    return this;
  }

  private addWins() {
    this.wins++;
  }

  private addDraws() {
    this.draws++;
  }

  private addLosses() {
    this.losses++;
  }

  addGoalDifference(points: number) {
    this.goalDifference += points;
    return this;
  }

  addGoalsScored(points: number) {
    this.goalsScored += points;
    return this;
  }

  addGoalsConceived(points: number) {
    this.goalsConceived += points;
    return this;
  }
}
