import { iTeam } from '../models';
import { Standing } from './standing';

export class Sort {
  standings(standings: Standing[]): Standing[] {
    return standings.sort((a, b) => {
      if (b.position !== a.position) {
        return a.position - b.position; // Inverted since lower is better
      }

      if (b.points === a.points && b.goalDifference === a.goalDifference) {
        return b.goalsScored - a.goalsScored;
      }

      if (b.points === a.points) {
        return b.goalDifference - a.goalDifference;
      }

      return b.points - a.points;
    });
  }

  teamsOnReputation(teams: iTeam[]) {
    return teams.sort((a, b) => b.strength - a.strength);
  }

  teamsOnPerformance(teams: iTeam[], combinedGroupStandings: Standing[]) {
    if (combinedGroupStandings.length === 0) {
      return teams;
    }

    const filteredStandings = combinedGroupStandings.filter(
      (standing) => teams.indexOf(standing.team) !== -1
    );
    return filteredStandings.map((standing) => standing.team);
  }

  teamsRandomized(teams: iTeam[]) {
    let currentIndex = teams.length;
    let temporaryValue: iTeam;
    let randomIndex: number;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = teams[currentIndex];
      teams[currentIndex] = teams[randomIndex];
      teams[randomIndex] = temporaryValue;
    }
    return teams;
  }
}
