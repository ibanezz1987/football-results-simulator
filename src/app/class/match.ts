import { iTeam, MatchFases } from '../models';

export class Match {
  teams: iTeam[];
  result: number[] = [];
  extraTimeResult: number[] = [];
  penaltyResult: number[] = [];
  tieBreakType: MatchFases;
  decidedDuring = MatchFases.RegularTime;

  constructor(team1: iTeam, team2: iTeam, tieBrakeType: MatchFases) {
    this.teams = [team1, team2];
    this.tieBreakType = tieBrakeType;
  }
}
