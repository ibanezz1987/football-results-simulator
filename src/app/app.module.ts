import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DisplayComponent } from './display/display.component';
import { TableComponent } from './display/table/table.component';
import { KnockoutComponent } from './display/knockout/knockout.component';
import { TeamsComponent } from './display/teams/teams.component';
import { HomeComponent } from './page/home/home.component';
import { CompetitionComponent } from './page/competition/competition.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DisplayComponent,
    TableComponent,
    KnockoutComponent,
    TeamsComponent,
    HomeComponent,
    CompetitionComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
