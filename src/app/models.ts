import { Match } from './class/match';
import { Standing } from './class/standing';

export interface iTeam {
  id: number;
  name: string;
  strength: number;
  // TODO: add array to add custom region strings, that the code can match on (one or multiple)
  //       In team: regions: ['Europe', 'Netherlands', 'Noord Holland', 'Amsterdam']
  //       In league: regions: ['Netherlands', 'Belgium']
  continent: Continent;
  nation?: string;
  isNationalTeam?: boolean;
}

export enum Continent {
  NorthAmerica = 'North America',
  SouthAmerica = 'South America',
  Europe = 'Europe',
  Africa = 'Africa',
  Asia = 'Asia',
  Oceania = 'Oceania',
}

export enum MathRoundType {
  RawValue = 'Raw value',
  Round = 'Round',
  RoundUp = 'Round up',
  RoundDown = 'Round down',
  Round1Decimal = 'Round to 1 decimal',
  Round2Decimals = 'Round to 2 decimals',
  Round3Decimals = 'Round to 3 decimals',
}

export enum DrawType {
  Random = 'Random',
  Reputation = 'Reputation',
  Performance = 'Performance',
  EqualPerformance = 'Equal performance',
}

export enum ScheduleType {
  Regular = 'Regular',
  CrossSubLeagues = 'Cross sub leagues',
  InterSubLeagues = 'Inter sub leagues',
}

export enum MatchFases {
  RegularTime = 'Regular time',
  ExtraTime = 'Extra time',
  Penalties = 'Penalties',
}

interface iSubLeague {
  teamIds: number[];
}

// TODO: remove iStageSettings
export interface iStageSettings {
  teams?: iTeam[];
  teamIds?: number[];
  subLeagues?: iSubLeague[];
  scheduleType?: ScheduleType;
}

export interface iCompetitionStageSettings {
  name: string | string[];
  stageSettings?: iStageSettings;
  teamsEntering?: number;
  teamsSkipping?: number;
  totalGroups?: number;
  rounds?: number;
  matchDaysLimit?: number;
  previousRoundPointsFactor?: number;
  pointRoundingStrategy?: MathRoundType;
  drawType?: DrawType;
  tiebrakeBy?: MatchFases;
}

export interface iCompetitionStageData extends iCompetitionStageSettings {
  standings?: Standing[][];
  combinedGroupStandings?: Standing[];
  schedule?: Match[][];
  skippingTeams?: iTeam[];
}

export interface iCompetitionSettings {
  id: number;
  name: string;
  stages: iCompetitionStageSettings[];
}

interface iIncludedTeamsFromOtherCompetition {
  sourceIndex: number;
  targetIndex: number;
  teamsEntering: number;
}

export interface iLeagueSystem {
  id: number;
  name: string;
  competitions: iCompetitionSettings[];
  includedTeamsFromOtherCompetition?: iIncludedTeamsFromOtherCompetition[];
}
