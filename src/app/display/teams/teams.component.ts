import { Component, Input } from '@angular/core';
import { iTeam } from 'src/app/models';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss'],
})
export class TeamsComponent {
  @Input() teams: iTeam[];
}
