import { Component, Input, OnInit } from '@angular/core';
import { Match } from 'src/app/class/match';
import { Standing } from 'src/app/class/standing';
import { MatchFases } from 'src/app/models';

@Component({
  selector: 'app-knockout',
  templateUrl: './knockout.component.html',
  styleUrls: ['./knockout.component.scss'],
})
export class KnockoutComponent implements OnInit {
  @Input() schedule: Match[];
  @Input() standings: Standing[];
  isAdvancing: boolean[] = [];

  ngOnInit() {
    if (!this.schedule.length) {
      return;
    }
    this.schedule[0].teams.forEach((team, index) => {
      const standing = this.standings.find(
        (standing) => standing.team.id === team.id
      );
      this.isAdvancing[index] = standing.isAdvancing;
    });
  }

  hasWonDuringExtraTime(match: Match, teamIndex: number) {
    const decidedDuringExtraTime = match.decidedDuring === MatchFases.ExtraTime;
    const otherIndex = teamIndex === 0 ? 1 : 0;
    const hasScoredMoreGoals =
      match.extraTimeResult[teamIndex] > match.extraTimeResult[otherIndex];
    return decidedDuringExtraTime && hasScoredMoreGoals;
  }

  hasWonDuringPenalties(match: Match, teamIndex: number) {
    const decidedByPenalties = match.decidedDuring === MatchFases.Penalties;
    const otherIndex = teamIndex === 0 ? 1 : 0;
    const hasScoredMorePenalties =
      match.penaltyResult[teamIndex] > match.penaltyResult[otherIndex];
    return decidedByPenalties && hasScoredMorePenalties;
  }

  isDecidedDuringRegularTime(match: Match) {
    return match.decidedDuring === MatchFases.RegularTime;
  }

  hasTieBrakeTypePenalties(match: Match) {
    return match.tieBreakType === MatchFases.Penalties;
  }
}
