import { Component, Input } from '@angular/core';
import { iCompetitionStageData, ScheduleType } from 'src/app/models';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss'],
})
export class DisplayComponent {
  @Input() competitionData: iCompetitionStageData[];

  isKnockoutStage(stage: iCompetitionStageData) {
    const teamsSkipping = stage.teamsSkipping ?? 0;
    const teams = stage.stageSettings?.teams?.length;
    const teamsEntering = teams ? teams - teamsSkipping : stage.teamsEntering;
    const totalGroups = stage.totalGroups ?? 1;
    const usesPreviousPoints = !!stage.previousRoundPointsFactor;
    return teamsEntering / totalGroups === 2 && !usesPreviousPoints;
  }

  getStageName(stage: iCompetitionStageData) {
    if (typeof stage.name === 'string') {
      return stage.name;
    }
    if (!stage.name.length) {
      return '';
    }
    return stage.name[0];
  }

  getSubStageName(
    stage: iCompetitionStageData,
    index: number,
    totalItems: number,
    isKnockoutStage = false
  ) {
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const hasIgnoredStage = this.isSubLeagueThatCanBeIgnored(stage, 0);
    index = hasIgnoredStage ? index - 1 : index;
    if (
      !stage.name.length ||
      (this.competitionData.length === 1 && stage.schedule.length === 1)
    ) {
      return '';
    }
    if (typeof stage.name !== 'string') {
      return stage.name[index + 1] ?? '';
    }
    if (isKnockoutStage) {
      return stage.name;
    }
    if (totalItems === 1) {
      return '';
    }
    const useLettersForGroupNumbering = index < alphabet.length;
    return `Group ${useLettersForGroupNumbering ? alphabet[index] : index + 1}`;
  }

  isSubLeagueThatCanBeIgnored(stage: iCompetitionStageData, i: number) {
    const scheduleType = stage.stageSettings?.scheduleType;
    return i === 0 && scheduleType && scheduleType !== ScheduleType.Regular;
  }
}
