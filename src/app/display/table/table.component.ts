import { Component, Input } from '@angular/core';
import { Standing } from 'src/app/class/standing';
import { MathRoundType } from 'src/app/models';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent {
  @Input() title: string;
  @Input() standings: Standing[];
  @Input() pointRoundingStrategy: MathRoundType;

  getDisplayPoints(points: number) {
    switch (this.pointRoundingStrategy) {
      case MathRoundType.Round:
      case MathRoundType.RoundUp:
      case MathRoundType.RoundDown:
        return this.parseValue(points, 0);

      case MathRoundType.Round1Decimal:
        return this.parseValue(points, 1);

      case MathRoundType.Round2Decimals:
        return this.parseValue(points, 2);

      case MathRoundType.Round3Decimals:
        return this.parseValue(points, 3);

      case MathRoundType.RawValue:
        return this.parseValue(points, 10);

      default:
        return points;
    }
  }

  parseValue(points: number, decimals: number) {
    return parseFloat(String(points)).toFixed(decimals);
  }
}
