import {
  Continent,
  MathRoundType,
  DrawType,
  iCompetitionSettings,
  iCompetitionStageSettings,
  MatchFases,
  iLeagueSystem,
  ScheduleType,
} from '../models';
import { clubTeams } from './clubTeams';
import { nationalTeams } from './nationalTeams';
import {
  tutorialCompetitions,
  tutorialLeagueSystems,
} from './tutorial-competitions';

const clubWorldCupTeamsIds = [1001, 1166, 1163, 1164, 1165, 1161, 1162];

const futureClubWorldCupTeamsIds = [
  1001, 1166, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1162, 1163, 1164, 1165,
  1167, 1168, 1169, 1170, 1171, 1172, 1173, 1174, 1175, 1176, 1161, 1177,
];

//
// Teams:
//

let filteredNations = [];
const singleBestTeamsFromUniqueNations = clubTeams.filter((team) => {
  if (filteredNations.indexOf(team.nation) === -1) {
    filteredNations.push(team.nation);
    return team;
  }
});
const worldNationalTeams = nationalTeams.sort(
  (a, b) => b.strength - a.strength
);
const europeanNationalTeams = nationalTeams.filter(
  (team) => team.continent === Continent.Europe
);
const southAmericanNationalTeams = nationalTeams.filter(
  (team) => team.continent === Continent.SouthAmerica
);
const unevenSelectedNationalTeams = [
  ...nationalTeams.filter((team) => team.strength > 80).slice(0, 1),
  ...nationalTeams
    .filter((team) => team.strength < 15 && team.strength > 10)
    .slice(0, 1),
  ...nationalTeams.filter((team) => team.strength <= 1).slice(0, 1),
];
const belgianTeams = [...clubTeams.filter((team) => team.nation === 'Bel')];
const dutchTeams = [...clubTeams.filter((team) => team.nation === 'Net')];
const germanTeams = [...clubTeams.filter((team) => team.nation === 'Ger')];

//
// Competition parts:
//

const defaultFinal: iCompetitionStageSettings = {
  name: 'Final',
  teamsEntering: 2,
  rounds: 1,
  tiebrakeBy: MatchFases.ExtraTime,
};
const defaultSemiFinal: iCompetitionStageSettings = {
  name: 'Semi Finals',
  totalGroups: 2,
  teamsEntering: 4,
  tiebrakeBy: MatchFases.ExtraTime,
};
const defaultQuarterFinal: iCompetitionStageSettings = {
  name: 'Quarter Finals',
  totalGroups: 4,
  teamsEntering: 8,
  tiebrakeBy: MatchFases.ExtraTime,
};
const defaultRoundOf16: iCompetitionStageSettings = {
  name: 'Round of 16',
  totalGroups: 8,
  teamsEntering: 16,
  tiebrakeBy: MatchFases.ExtraTime,
};

const defaultFinalWithThirdPlacePlayoff: iCompetitionStageSettings = {
  name: ['Finals', 'Final', 'Third Place Play-off'],
  totalGroups: 2,
  teamsEntering: 4,
  rounds: 1,
  drawType: DrawType.EqualPerformance,
  tiebrakeBy: MatchFases.ExtraTime,
};
const singleRoundSemiFinal: iCompetitionStageSettings = {
  ...defaultSemiFinal,
  rounds: 1,
  tiebrakeBy: MatchFases.ExtraTime,
};
const singleRoundQuarterFinal: iCompetitionStageSettings = {
  ...defaultQuarterFinal,
  rounds: 1,
  tiebrakeBy: MatchFases.ExtraTime,
};

//
// Competitions:
//

const worldCup: iCompetitionSettings = {
  id: 1,
  name: 'World Cup',
  stages: [
    {
      name: 'Group Stage',
      totalGroups: 8,
      rounds: 1,
      drawType: DrawType.Reputation,
      stageSettings: {
        teamIds: [...worldNationalTeams.slice(0, 32).map((team) => team.id)],
      },
    },
    {
      ...get(defaultRoundOf16),
      drawType: DrawType.Performance,
      rounds: 1,
    },
    get(singleRoundQuarterFinal),
    get(singleRoundSemiFinal),
    get(defaultFinalWithThirdPlacePlayoff),
  ],
};

const olympicFootballTournament: iCompetitionSettings = {
  id: 2,
  name: 'Olympic Football Tournament',
  stages: [
    {
      name: 'Group Stage',
      totalGroups: 4,
      rounds: 1,
      drawType: DrawType.Reputation,
      stageSettings: {
        teamIds: [...worldNationalTeams.slice(0, 16).map((team) => team.id)],
      },
    },
    get(singleRoundQuarterFinal),
    get(singleRoundSemiFinal),
    get(defaultFinalWithThirdPlacePlayoff),
  ],
};

const nationalTeamWorldLeague: iCompetitionSettings = {
  id: 3,
  name: 'National Team World League',
  stages: [
    {
      name: 'Group Stage',
      rounds: 1,
      stageSettings: {
        teamIds: worldNationalTeams.map((team) => team.id),
      },
    },
  ],
};

const nationalTeamUnevenLeague: iCompetitionSettings = {
  id: 5,
  name: 'National Team Uneven League',
  stages: [
    {
      name: 'Group Stage',
      rounds: 10,
      stageSettings: {
        teamIds: unevenSelectedNationalTeams.map((team) => team.id),
      },
    },
  ],
};

const europeanChampionshipPreviousVersion: iCompetitionSettings = {
  id: 6,
  name: 'European Championship Previous Version',
  stages: [
    {
      name: 'Group Stage',
      totalGroups: 4,
      drawType: DrawType.Reputation,
      rounds: 1,
      stageSettings: {
        teamIds: [...europeanNationalTeams.slice(0, 16).map((team) => team.id)],
      },
    },
    get(singleRoundQuarterFinal),
    get(singleRoundSemiFinal),
    get(defaultFinal),
  ],
};

const europeanChampionship: iCompetitionSettings = {
  id: 7,
  name: 'European Championship',
  stages: [
    {
      name: 'Group Stage',
      totalGroups: 6,
      drawType: DrawType.Reputation,
      rounds: 1,
      stageSettings: {
        teamIds: [...europeanNationalTeams.slice(0, 24).map((team) => team.id)],
      },
    },
    {
      ...get(defaultRoundOf16),
      drawType: DrawType.Performance,
      rounds: 1,
    },
    get(singleRoundQuarterFinal),
    get(singleRoundSemiFinal),
    get(defaultFinal),
  ],
};

const nationsLeague: iCompetitionSettings = {
  id: 8,
  name: 'Nations League',
  stages: [
    {
      name: 'Group Stage League A',
      totalGroups: 4,
      drawType: DrawType.Reputation,
      stageSettings: {
        teamIds: [...europeanNationalTeams.slice(0, 16).map((team) => team.id)],
      },
    },
    {
      ...get(defaultSemiFinal),
      rounds: 1,
    },
    get(defaultFinalWithThirdPlacePlayoff),
  ],
};

const copaAmerica: iCompetitionSettings = {
  id: 9,
  name: 'Copa America',
  stages: [
    {
      name: ['Group Stage', 'Group A (South Zone)', 'Group B (North Zone)'],
      rounds: 1,
      drawType: DrawType.Reputation,
      stageSettings: {
        teamIds: [
          ...southAmericanNationalTeams.slice(0, 10).map((team) => team.id),
        ],
        subLeagues: [
          { teamIds: [57, 65, 58, 60, 63] },
          { teamIds: [59, 56, 62, 64, 61] },
        ],
        scheduleType: ScheduleType.InterSubLeagues,
      },
    },
    {
      ...get(defaultQuarterFinal),
      drawType: DrawType.Performance,
      rounds: 1,
    },
    {
      ...get(defaultSemiFinal),
      rounds: 1,
    },
    get(defaultFinalWithThirdPlacePlayoff),
  ],
};

const championsLeagueOriginalVersion: iCompetitionSettings = {
  id: 10,
  name: 'Champions League Original Version',
  stages: [
    {
      ...get(defaultRoundOf16),
      stageSettings: {
        teamIds: [
          ...singleBestTeamsFromUniqueNations
            .slice(0, 16)
            .map((team) => team.id),
        ],
      },
    },
    get(defaultQuarterFinal),
    get(defaultSemiFinal),
    get(defaultFinal),
  ],
};

const championsLeaguePreviousVersion: iCompetitionSettings = {
  id: 11,
  name: 'Champions League Previous Version',
  stages: [
    {
      name: 'First Group Stage',
      totalGroups: 8,
      drawType: DrawType.Reputation,
      stageSettings: {
        teamIds: [...clubTeams.slice(0, 32).map((team) => team.id)],
      },
    },
    {
      name: 'Second Group Stage',
      totalGroups: 4,
      teamsEntering: 16,
      drawType: DrawType.Performance,
    },
    {
      ...get(defaultQuarterFinal),
      drawType: DrawType.Performance,
    },
    get(defaultSemiFinal),
    get(defaultFinal),
  ],
};

const championsLeagueCurrentVersion: iCompetitionSettings = {
  id: 12,
  name: 'Champions League Current Version',
  stages: [
    {
      name: 'Group Stage',
      totalGroups: 8,
      drawType: DrawType.Reputation,
      stageSettings: {
        teamIds: [...clubTeams.slice(0, 32).map((team) => team.id)],
      },
    },
    {
      ...get(defaultRoundOf16),
      drawType: DrawType.Performance,
    },
    get(defaultQuarterFinal),
    get(defaultSemiFinal),
    get(defaultFinal),
  ],
};

const championsLeagueFutureVersion: iCompetitionSettings = {
  id: 13,
  name: 'Champions League Future Version',
  stages: [
    {
      name: ['Group Stage', 'Standings'],
      drawType: DrawType.Reputation,
      matchDaysLimit: 10,
      stageSettings: {
        teamIds: [...clubTeams.slice(0, 36).map((team) => team.id)],
      },
    },
    {
      name: 'Play-offs',
      drawType: DrawType.Performance,
      totalGroups: 8,
      teamsEntering: 16,
      teamsSkipping: 8,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    {
      ...get(defaultRoundOf16),
      drawType: DrawType.Performance,
    },
    get(defaultQuarterFinal),
    get(defaultSemiFinal),
    get(defaultFinal),
  ],
};

const clubWorldCup: iCompetitionSettings = {
  id: 36,
  name: 'Club World Cup Current Version',
  stages: [
    {
      name: 'First Round',
      totalGroups: 1,
      rounds: 1,
      teamsSkipping: 5,
      tiebrakeBy: MatchFases.ExtraTime,
      stageSettings: {
        teamIds: clubWorldCupTeamsIds,
      },
    },
    {
      name: 'Second Round',
      totalGroups: 2,
      rounds: 1,
      teamsEntering: 4,
      teamsSkipping: 2,
      tiebrakeBy: MatchFases.ExtraTime,
      drawType: DrawType.Reputation,
    },
    {
      name: 'Semi Final',
      totalGroups: 2,
      rounds: 1,
      teamsEntering: 4,
      tiebrakeBy: MatchFases.ExtraTime,
      drawType: DrawType.Reputation,
    },
    get(defaultFinalWithThirdPlacePlayoff),
  ],
};

const futureClubWorldCup: iCompetitionSettings = {
  id: 37,
  name: 'Club World Cup Future Version',
  stages: [
    {
      name: 'Qualification Round',
      totalGroups: 1,
      rounds: 1,
      teamsSkipping: 23,
      tiebrakeBy: MatchFases.ExtraTime,
      stageSettings: {
        teamIds: futureClubWorldCupTeamsIds,
      },
    },
    {
      name: 'Group Stage',
      totalGroups: 8,
      rounds: 1,
      teamsEntering: 24,
      drawType: DrawType.Reputation,
      stageSettings: {
        teamIds: futureClubWorldCupTeamsIds,
      },
    },
    get(singleRoundQuarterFinal),
    get(singleRoundSemiFinal),
    get(defaultFinalWithThirdPlacePlayoff),
  ],
};

const theSuperLeague: iCompetitionSettings = {
  id: 14,
  name: 'The Super League',
  stages: [
    {
      name: 'Group Stage',
      totalGroups: 2,
      drawType: DrawType.Reputation,
      stageSettings: {
        teamIds: [...clubTeams.slice(0, 20).map((team) => team.id)],
      },
    },
    {
      name: 'Play-offs',
      drawType: DrawType.Performance,
      totalGroups: 2,
      teamsEntering: 4,
      teamsSkipping: 6,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    {
      ...get(defaultQuarterFinal),
      drawType: DrawType.Performance,
    },
    get(defaultSemiFinal),
    get(defaultFinal),
  ],
};

const bedeneliga: iCompetitionSettings = {
  id: 16,
  name: 'Bedeneliga',
  stages: [
    {
      name: [
        'Group Stage',
        'Combined standings',
        'Belgian First Division',
        'Eredivisie',
        'German Liga',
      ],
      stageSettings: {
        teamIds: [
          ...belgianTeams.slice(0, 5).map((team) => team.id),
          ...dutchTeams.slice(0, 5).map((team) => team.id),
          ...germanTeams.slice(0, 6).map((team) => team.id),
        ],
        subLeagues: [
          { teamIds: belgianTeams.map((team) => team.id) },
          { teamIds: dutchTeams.map((team) => team.id) },
          { teamIds: germanTeams.map((team) => team.id) },
        ],
      },
    },
    {
      ...get(defaultSemiFinal),
      drawType: DrawType.Performance,
    },
    get(defaultFinal),
  ],
};

const beneligaSplittedLeague: iCompetitionSettings = {
  id: 18,
  name: 'Beneliga Splitted League',
  stages: [
    {
      name: [
        'Group Stage',
        'Combined standings',
        'Belgian First Division',
        'Eredivisie',
      ],
      stageSettings: {
        teamIds: [
          ...belgianTeams.slice(0, 8).map((team) => team.id),
          ...dutchTeams.slice(0, 8).map((team) => team.id),
        ],
        subLeagues: [
          { teamIds: belgianTeams.map((team) => team.id) },
          { teamIds: dutchTeams.map((team) => team.id) },
        ],
      },
    },
    {
      ...get(defaultSemiFinal),
      drawType: DrawType.Performance,
    },
    get(defaultFinal),
  ],
};

const beneligaSplittedLeagueWithGrandFinal: iCompetitionSettings = {
  id: 19,
  name: 'Beneliga Splitted League With Grand Final',
  stages: [
    {
      name: [
        'Group Stage',
        'Combined standings',
        'Belgian Standings',
        'Dutch Standings',
      ],
      stageSettings: {
        teamIds: [
          ...belgianTeams.slice(0, 8).map((team) => team.id),
          ...dutchTeams.slice(0, 8).map((team) => team.id),
        ],
        subLeagues: [
          { teamIds: belgianTeams.map((team) => team.id) },
          { teamIds: dutchTeams.map((team) => team.id) },
        ],
      },
    },
    {
      ...get(defaultSemiFinal),
      name: 'Quarter Finals',
      teamsSkipping: 2,
      drawType: DrawType.Performance,
    },
    {
      ...get(defaultSemiFinal),
    },
    get(defaultFinal),
  ],
};

const dutchPremierDivision: iCompetitionSettings = {
  id: 21,
  name: 'Eredivisie',
  stages: [
    {
      name: 'Group Stage',
      stageSettings: {
        teamIds: [...dutchTeams.slice(0, 18).map((team) => team.id)],
      },
    },
  ],
};

const dutchPremierDivisionWithPlayoffs: iCompetitionSettings = {
  id: 26,
  name: 'Eredivisie with playoffs',
  stages: [
    {
      name: 'Group Stage',
      stageSettings: {
        teamIds: [...dutchTeams.slice(0, 18).map((team) => team.id)],
      },
    },
    {
      name: [
        'Play-offs Semi Finals',
        'Play-offs Semi Final',
        'Play-offs Semi Final',
      ],
      totalGroups: 2,
      rounds: 1,
      teamsSkipping: 3,
      teamsEntering: 4,
      drawType: DrawType.Performance,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    {
      name: ['Play-offs Final', 'Play-offs Final'],
      rounds: 1,
      teamsSkipping: 3,
      teamsEntering: 2,
      tiebrakeBy: MatchFases.ExtraTime,
    },
  ],
};

const dutchPremierDivisionKnockOut: iCompetitionSettings = {
  id: 22,
  name: 'Eredivisie Battle Royale',
  stages: [
    {
      name: 'Round of 8',
      rounds: 1,
      stageSettings: {
        teamIds: [...dutchTeams.slice(0, 8).map((team) => team.id)],
      },
    },
    {
      name: 'Round of 7',
      teamsEntering: 7,
      rounds: 1,
      previousRoundPointsFactor: 0.5,
      pointRoundingStrategy: MathRoundType.Round,
    },
    {
      name: 'Round of 6',
      teamsEntering: 6,
      rounds: 1,
      previousRoundPointsFactor: 0.5,
      pointRoundingStrategy: MathRoundType.Round,
    },
    {
      name: 'Round of 5',
      teamsEntering: 5,
      rounds: 1,
      previousRoundPointsFactor: 0.5,
      pointRoundingStrategy: MathRoundType.Round,
    },
    {
      name: 'Round of 4',
      teamsEntering: 4,
      rounds: 1,
      previousRoundPointsFactor: 0.5,
      pointRoundingStrategy: MathRoundType.Round,
    },
    {
      name: 'Round of 3',
      teamsEntering: 3,
      rounds: 1,
      previousRoundPointsFactor: 0.5,
      pointRoundingStrategy: MathRoundType.Round,
    },
    {
      name: 'Round of 2',
      teamsEntering: 2,
      rounds: 1,
      previousRoundPointsFactor: 0.5,
      pointRoundingStrategy: MathRoundType.Round,
    },
  ],
};

const dutchPremierDivisionWithGrandFinalKnockOut: iCompetitionSettings = {
  id: 23,
  name: 'Eredivisie With Grand Final Knock Out',
  stages: [
    ...get(dutchPremierDivision.stages),
    {
      name: 'Quarter Final',
      teamsEntering: 2,
      teamsSkipping: 2,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    {
      name: 'Semi Final',
      teamsEntering: 2,
      teamsSkipping: 1,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    get(defaultFinal),
  ],
};

const dutchPremierDivisionWithSafetyNetKnockOut: iCompetitionSettings = {
  id: 24,
  name: 'Eredivisie With Safety Net Knock Out',
  stages: [
    ...get(dutchPremierDivision.stages),
    {
      name: ['Quarter Finals', 'Upper Quarter Final', 'Lower Quarter Final'],
      teamsEntering: 4,
      totalGroups: 2,
      teamsSkipping: 1,
      rounds: 1,
      drawType: DrawType.EqualPerformance,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    {
      name: ['Semi Finals', 'Upper Semi Final', 'Lower Semi Final'],
      teamsEntering: 4,
      totalGroups: 2,
      rounds: 1,
      drawType: DrawType.EqualPerformance,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    {
      name: 'Preliminary Final',
      teamsEntering: 2,
      teamsSkipping: 1,
      rounds: 1,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    {
      name: 'Grand Final',
      teamsEntering: 2,
      rounds: 1,
      tiebrakeBy: MatchFases.ExtraTime,
    },
  ],
};

const dutchCup: iCompetitionSettings = {
  id: 25,
  name: 'Dutch Cup',
  stages: [
    {
      name: 'Round of 32',
      rounds: 1,
      totalGroups: 16,
      tiebrakeBy: MatchFases.ExtraTime,
      stageSettings: {
        teamIds: [...dutchTeams.slice(0, 32).map((team) => team.id)],
      },
    },
    { ...get(defaultRoundOf16), rounds: 1 },
    { ...get(defaultQuarterFinal), rounds: 1 },
    { ...get(defaultSemiFinal), rounds: 1 },
    get(defaultFinal),
  ],
};

const belgianLeague: iCompetitionSettings = {
  id: 31,
  name: 'Belgian First Division',
  stages: [
    {
      name: 'Regular Standings',
      drawType: DrawType.Reputation,
      stageSettings: {
        teamIds: [...belgianTeams.slice(0, 18).map((team) => team.id)],
      },
    },
    {
      name: ['Play-offs', 'Championship Play-offs', 'Europa Play-offs'],
      drawType: DrawType.EqualPerformance,
      totalGroups: 2,
      teamsEntering: 8,
      previousRoundPointsFactor: 0.5,
      pointRoundingStrategy: MathRoundType.Round,
    },
  ],
};

const belgianCup: iCompetitionSettings = {
  id: 32,
  name: 'Belgian Cup',
  stages: [
    {
      name: 'Round of 32',
      rounds: 1,
      totalGroups: 16,
      tiebrakeBy: MatchFases.ExtraTime,
      stageSettings: {
        teamIds: [...belgianTeams.slice(0, 32).map((team) => team.id)],
      },
    },
    { ...get(defaultRoundOf16), rounds: 1 },
    { ...get(defaultQuarterFinal), rounds: 1 },
    { ...get(defaultSemiFinal), rounds: 1 },
    get(defaultFinal),
  ],
};

const beneCup: iCompetitionSettings = {
  id: 33,
  name: 'BeneCup',
  stages: [
    {
      name: 'League Standings',
      rounds: 1,
      stageSettings: {
        teamIds: [
          ...belgianTeams.slice(0, 3).map((team) => team.id),
          ...dutchTeams.slice(0, 3).map((team) => team.id),
        ],
        subLeagues: [
          { teamIds: belgianTeams.map((team) => team.id) },
          { teamIds: dutchTeams.map((team) => team.id) },
        ],
        scheduleType: ScheduleType.CrossSubLeagues,
      },
    },
    { ...get(defaultSemiFinal), rounds: 1 },
    get(defaultFinal),
  ],
};

const kampioenschapDerLageLanden: iCompetitionSettings = {
  id: 34,
  name: 'Kampioenschap der lage landen',
  stages: [
    {
      name: 'Result',
      rounds: 1,
      stageSettings: {
        teamIds: [
          ...belgianTeams.slice(0, 1).map((team) => team.id),
          ...dutchTeams.slice(0, 1).map((team) => team.id),
        ],
      },
      tiebrakeBy: MatchFases.ExtraTime,
    },
  ],
};

const bekerDerLageLanden: iCompetitionSettings = {
  id: 38,
  name: 'Beker der lage landen',
  stages: [
    {
      name: 'Result',
      rounds: 1,
      stageSettings: {
        teamIds: [
          ...belgianTeams.slice(0, 1).map((team) => team.id),
          ...dutchTeams.slice(0, 1).map((team) => team.id),
        ],
      },
      tiebrakeBy: MatchFases.ExtraTime,
    },
  ],
};

const royalLeague: iCompetitionSettings = {
  id: 39,
  name: 'Johan Cruijff Shield',
  stages: [
    {
      name: ['League Standings', 'Belgian Standings', 'Dutch Standings'],
      rounds: 1,
      stageSettings: {
        teamIds: [
          ...belgianTeams.slice(0, 4).map((team) => team.id),
          ...dutchTeams.slice(0, 4).map((team) => team.id),
        ],
        subLeagues: [
          { teamIds: belgianTeams.map((team) => team.id) },
          { teamIds: dutchTeams.map((team) => team.id) },
        ],
        scheduleType: ScheduleType.CrossSubLeagues,
      },
    },
    {
      name: ['Semi Finals', 'Belgian Semi Final', 'Dutch Semi Final'],
      rounds: 1,
      teamsEntering: 4,
      tiebrakeBy: MatchFases.ExtraTime,
      stageSettings: {
        teamIds: [
          ...belgianTeams.slice(0, 4).map((team) => team.id),
          ...dutchTeams.slice(0, 4).map((team) => team.id),
        ],
        subLeagues: [
          { teamIds: belgianTeams.map((team) => team.id) },
          { teamIds: dutchTeams.map((team) => team.id) },
        ],
        scheduleType: ScheduleType.InterSubLeagues,
      },
    },
    {
      name: ['Finals', 'Gold Final', 'Bronze Final'],
      totalGroups: 2,
      teamsEntering: 4,
      rounds: 1,
      drawType: DrawType.EqualPerformance,
      tiebrakeBy: MatchFases.ExtraTime,
    },
  ],
};

const dutchPremierDivisionCondensed: iCompetitionSettings = {
  id: 40,
  name: 'Eredivisie',
  stages: [
    {
      name: 'Opening Round',
      rounds: 1,
      stageSettings: {
        teamIds: [...dutchTeams.slice(0, 16).map((team) => team.id)],
      },
    },
    {
      name: ['Closing Round', 'Championship League', 'Challenge League'],
      drawType: DrawType.EqualPerformance,
      totalGroups: 2,
      teamsEntering: 16,
      previousRoundPointsFactor: 1,
    },
  ],
};

const brabantCup: iCompetitionSettings = {
  id: 42,
  name: 'Brabants kampioenschap',
  stages: [
    {
      name: 'League standings',
      rounds: 1,
      stageSettings: {
        teamIds: [1056, 1110, 1111, 1119, 1124, 1126, 1132],
      },
    },
    defaultFinal,
  ],
};

const limburgCup: iCompetitionSettings = {
  id: 43,
  name: 'Limburgs kampioenschap',
  stages: [
    {
      name: 'League standings',
      stageSettings: {
        teamIds: [1108, 1113, 1125, 1122],
      },
    },
    defaultFinal,
  ],
};

const netherlandsNorthCup: iCompetitionSettings = {
  id: 44,
  name: 'Kampioenschap van het noorden',
  stages: [
    {
      name: 'League standings',
      stageSettings: {
        teamIds: [1104, 1105, 1112, 1115],
      },
    },
    defaultFinal,
  ],
};

const northHollandCup: iCompetitionSettings = {
  id: 45,
  name: 'Kampioenschap van Noord Holland (e.o.)',
  stages: [
    {
      name: 'League standings',
      rounds: 1,
      stageSettings: {
        teamIds: [1017, 1066, 1120, 1102, 1118, 1127],
      },
    },
    { ...singleRoundSemiFinal, drawType: DrawType.Performance },
    defaultFinal,
  ],
};

const southHollandCup: iCompetitionSettings = {
  id: 46,
  name: 'Kampioenschap van Zuid Holland',
  stages: [
    {
      name: 'League standings',
      rounds: 1,
      stageSettings: {
        teamIds: [1068, 1106, 1114, 1123, 1133],
      },
    },
    { ...defaultSemiFinal, drawType: DrawType.Performance },
    defaultFinal,
  ],
};

const eastNetherlandsCup: iCompetitionSettings = {
  id: 47,
  name: 'Kampioenschap van het oosten',
  stages: [
    {
      name: 'League standings',
      rounds: 1,
      stageSettings: {
        teamIds: [1101, 1117, 1121, 1103, 1107, 1109, 1116],
      },
    },
    defaultFinal,
  ],
};

const dutchPremierDivisionYouth: iCompetitionSettings = {
  id: 48,
  name: 'Kleine Eredivisie',
  stages: [
    {
      name: 'League standings',
      stageSettings: {
        teamIds: [1128, 1129, 1130, 1131],
      },
    },
    defaultFinal,
  ],
};

const dutchChampionsCup: iCompetitionSettings = {
  id: 49,
  name: 'Beker der Kampioenen',
  stages: [
    {
      name: [
        'First Round',
        'Noord/Oost Beker',
        'Beker van Holland',
        'Beker van het zuiden',
      ],
      rounds: 1,
      tiebrakeBy: MatchFases.ExtraTime,
      stageSettings: {
        scheduleType: ScheduleType.InterSubLeagues,
        teamIds: [
          1104, 1105, 1112, 1115, 1101, 1117, 1121, 1103, 1107, 1109, 1116,
          1017, 1066, 1120, 1102, 1118, 1127, 1068, 1106, 1114, 1123, 1133,
          1056, 1110, 1111, 1119, 1124, 1126, 1132, 1108, 1113, 1125, 1122,
        ],
        subLeagues: [
          {
            teamIds: [
              1104, 1105, 1112, 1115, 1101, 1117, 1121, 1103, 1107, 1109, 1116,
            ],
          },
          {
            teamIds: [
              1017, 1066, 1120, 1102, 1118, 1127, 1068, 1106, 1114, 1123, 1133,
            ],
          },
          {
            teamIds: [
              1056, 1110, 1111, 1119, 1124, 1126, 1132, 1108, 1113, 1125, 1122,
            ],
          },
        ],
      },
    },
    singleRoundSemiFinal,
    defaultFinal,
  ],
};

const beneleagues: iLeagueSystem = {
  id: 1001,
  name: 'Beneleagues',
  competitions: [
    get(belgianLeague),
    get(belgianCup),
    get(dutchPremierDivisionWithPlayoffs),
    get(dutchCup),
    get(kampioenschapDerLageLanden),
    get(bekerDerLageLanden),
    get(beneCup),
    get(royalLeague),
  ],
  includedTeamsFromOtherCompetition: [
    { sourceIndex: 0, targetIndex: 4, teamsEntering: 1 },
    { sourceIndex: 2, targetIndex: 4, teamsEntering: 1 },
    { sourceIndex: 1, targetIndex: 5, teamsEntering: 1 },
    { sourceIndex: 3, targetIndex: 5, teamsEntering: 1 },
    { sourceIndex: 0, targetIndex: 6, teamsEntering: 3 },
    { sourceIndex: 2, targetIndex: 6, teamsEntering: 3 },
    { sourceIndex: 0, targetIndex: 7, teamsEntering: 4 },
    { sourceIndex: 2, targetIndex: 7, teamsEntering: 4 },
  ],
};

const dutchPremierDivisionWithRegionalCups: iLeagueSystem = {
  id: 1002,
  name: 'Eredivisie with regional cups',
  competitions: [
    get(dutchPremierDivisionCondensed),
    get(dutchCup),
    get(netherlandsNorthCup),
    get(northHollandCup),
    get(southHollandCup),
    get(eastNetherlandsCup),
    get(brabantCup),
    get(limburgCup),
    get(dutchPremierDivisionYouth),
    get(dutchChampionsCup),
  ],
  includedTeamsFromOtherCompetition: [
    { sourceIndex: 2, targetIndex: 9, teamsEntering: 1 },
    { sourceIndex: 3, targetIndex: 9, teamsEntering: 1 },
    { sourceIndex: 4, targetIndex: 9, teamsEntering: 1 },
    { sourceIndex: 5, targetIndex: 9, teamsEntering: 1 },
    { sourceIndex: 6, targetIndex: 9, teamsEntering: 1 },
    { sourceIndex: 7, targetIndex: 9, teamsEntering: 1 },
  ],
};

const realWorldCompetitions = [
  worldCup,
  olympicFootballTournament,
  europeanChampionshipPreviousVersion,
  europeanChampionship,
  nationsLeague,
  copaAmerica,
  championsLeagueOriginalVersion,
  championsLeaguePreviousVersion,
  championsLeagueCurrentVersion,
  championsLeagueFutureVersion,
  clubWorldCup,
  futureClubWorldCup,
  theSuperLeague,
  dutchPremierDivisionWithPlayoffs,
  belgianLeague,
];

const fantasyCompetitions = [
  nationalTeamWorldLeague,
  nationalTeamUnevenLeague,
  bedeneliga,
  beneligaSplittedLeague,
  beneligaSplittedLeagueWithGrandFinal,
  dutchPremierDivisionKnockOut,
  dutchPremierDivisionWithGrandFinalKnockOut,
  dutchPremierDivisionWithSafetyNetKnockOut,
];

export const exampleCompetitions = [
  ...getLeaguesWithPrefixAndCounter(tutorialCompetitions, 'Tutorial'),
  ...getLeaguesWithPrefixAndCounter(realWorldCompetitions, 'Existing'),
  ...getLeaguesWithPrefixAndCounter(fantasyCompetitions, 'Fantasy'),
];

export const exampleLeagueSystems = [
  ...tutorialLeagueSystems,
  beneleagues,
  dutchPremierDivisionWithRegionalCups,
];

function getLeaguesWithPrefixAndCounter(
  competitions: iCompetitionSettings[],
  prefix: string
) {
  return competitions.map((competition, index) => ({
    ...competition,
    name: (competition.name = `${prefix} #${index + 1}: ${competition.name}`),
  }));
}

function get(competitionSettings) {
  return JSON.parse(JSON.stringify(competitionSettings));
}
