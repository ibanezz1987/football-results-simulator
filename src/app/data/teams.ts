import { iTeam } from '../models';
import { clubTeams } from './clubTeams';
import { nationalTeams } from './nationalTeams';

export const teamsList: iTeam[] = [...nationalTeams, ...clubTeams];
