import {
  Continent,
  DrawType,
  iCompetitionSettings,
  iLeagueSystem,
  MatchFases,
  MathRoundType,
  ScheduleType,
} from '../models';
import { nationalTeams } from './nationalTeams';

const worldNationalTeams = nationalTeams.sort(
  (a, b) => b.strength - a.strength
);
const europeanNationalTeams = nationalTeams.filter(
  (team) => team.continent === Continent.Europe
);
const northAmericanNationalTeams = nationalTeams.filter(
  (team) => team.continent === Continent.NorthAmerica
);
const southAmericanNationalTeams = nationalTeams.filter(
  (team) => team.continent === Continent.SouthAmerica
);
const asianNationalTeams = nationalTeams.filter(
  (team) => team.continent === Continent.Asia
);
const africanNationalTeams = nationalTeams.filter(
  (team) => team.continent === Continent.Africa
);
const oceanianNationalTeams = nationalTeams.filter(
  (team) => team.continent === Continent.Oceania
);

const groupAndKnockOutStages: iCompetitionSettings = {
  id: 100001,
  name: 'Group and knockout stages',
  stages: [
    {
      name: '1. 10 teams in 1 group',
      rounds: 1,
      stageSettings: {
        teamIds: [...worldNationalTeams.slice(0, 10).map((team) => team.id)],
      },
    },
    {
      name: '2. 8 teams distributed over 3 groups',
      rounds: 1,
      totalGroups: 3,
      teamsEntering: 8,
    },
    {
      name: ['3. 3 knockout matchups', '', '', ''],
      rounds: 1,
      totalGroups: 3,
      teamsEntering: 6,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    {
      name: ["4. Semi finals (1 lucky loser to semi's)", '', ''],
      rounds: 1,
      totalGroups: 2,
      teamsEntering: 4,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    {
      name: ['5. Final over 5 matches', ''],
      teamsEntering: 2,
      rounds: 5,
      tiebrakeBy: MatchFases.ExtraTime,
    },
  ],
};

const swissSystem: iCompetitionSettings = {
  id: 100002,
  name: 'Swiss system (match day limit)',
  stages: [
    {
      name: '1. Play each team 10 times',
      rounds: 10,
      stageSettings: {
        teamIds: [...worldNationalTeams.slice(0, 10).map((team) => team.id)],
      },
    },
    {
      name: '2. No limit on matches (1 full round)',
      rounds: 1,
      stageSettings: {
        teamIds: [...worldNationalTeams.slice(0, 10).map((team) => team.id)],
      },
    },
    {
      name: '3. Limit matches to 4',
      matchDaysLimit: 4,
      stageSettings: {
        teamIds: [...worldNationalTeams.slice(0, 10).map((team) => team.id)],
      },
    },
    {
      name: '4. Limit matches to 1',
      matchDaysLimit: 1,
    },
  ],
};

const subLeaguesBasics: iCompetitionSettings = {
  id: 100003,
  name: 'Sub leagues: Basics',
  stages: [
    {
      name: [
        '1: 6 sub leagues',
        'Combined standings',
        'European Group',
        'North American Group',
        'South American Group',
        'Asian Group',
        'African Group',
        'Oceanian Group',
      ],
      rounds: 1,
      stageSettings: {
        teamIds: [
          ...europeanNationalTeams.slice(0, 2).map((team) => team.id),
          ...northAmericanNationalTeams.slice(0, 2).map((team) => team.id),
          ...southAmericanNationalTeams.slice(0, 2).map((team) => team.id),
          ...asianNationalTeams.slice(0, 2).map((team) => team.id),
          ...africanNationalTeams.slice(0, 2).map((team) => team.id),
          ...oceanianNationalTeams.slice(0, 2).map((team) => team.id),
        ],
        subLeagues: [
          { teamIds: europeanNationalTeams.map((team) => team.id) },
          { teamIds: northAmericanNationalTeams.map((team) => team.id) },
          { teamIds: southAmericanNationalTeams.map((team) => team.id) },
          { teamIds: asianNationalTeams.map((team) => team.id) },
          { teamIds: africanNationalTeams.map((team) => team.id) },
          { teamIds: oceanianNationalTeams.map((team) => team.id) },
        ],
      },
    },
    {
      name: [
        '2: 2 sub leagues',
        'Combined standings',
        'Northern Group',
        'Southern Group',
      ],
      teamsEntering: 6,
      rounds: 1,
      stageSettings: {
        subLeagues: [
          {
            teamIds: [
              ...europeanNationalTeams.map((team) => team.id),
              ...northAmericanNationalTeams.map((team) => team.id),
              ...asianNationalTeams.map((team) => team.id),
            ],
          },
          {
            teamIds: [
              ...southAmericanNationalTeams.map((team) => team.id),
              ...africanNationalTeams.map((team) => team.id),
              ...oceanianNationalTeams.map((team) => team.id),
            ],
          },
        ],
      },
    },
  ],
};

const subLeaguesSchedules: iCompetitionSettings = {
  id: 100004,
  name: 'Sub leagues: Schedule options',
  stages: [
    {
      name: [
        '1: Regular schedule (play all)',
        'Combined standings',
        'Northern Group',
        'Southern Group',
      ],
      teamsEntering: 6,
      rounds: 1,
      stageSettings: {
        teamIds: [
          ...europeanNationalTeams.slice(0, 1).map((team) => team.id),
          ...northAmericanNationalTeams.slice(0, 1).map((team) => team.id),
          ...southAmericanNationalTeams.slice(0, 1).map((team) => team.id),
          ...asianNationalTeams.slice(0, 1).map((team) => team.id),
          ...africanNationalTeams.slice(0, 1).map((team) => team.id),
          ...oceanianNationalTeams.slice(0, 1).map((team) => team.id),
        ],
        subLeagues: [
          {
            teamIds: [
              ...europeanNationalTeams.map((team) => team.id),
              ...northAmericanNationalTeams.map((team) => team.id),
              ...asianNationalTeams.map((team) => team.id),
            ],
          },
          {
            teamIds: [
              ...southAmericanNationalTeams.map((team) => team.id),
              ...africanNationalTeams.map((team) => team.id),
              ...oceanianNationalTeams.map((team) => team.id),
            ],
          },
        ],
      },
    },
    {
      name: [
        '2: Play only against teams in the same group',
        'Northern Group',
        'Southern Group',
      ],
      teamsEntering: 6,
      rounds: 1,
      stageSettings: {
        scheduleType: ScheduleType.InterSubLeagues,
        subLeagues: [
          {
            teamIds: [
              ...europeanNationalTeams.map((team) => team.id),
              ...northAmericanNationalTeams.map((team) => team.id),
              ...asianNationalTeams.map((team) => team.id),
            ],
          },
          {
            teamIds: [
              ...southAmericanNationalTeams.map((team) => team.id),
              ...africanNationalTeams.map((team) => team.id),
              ...oceanianNationalTeams.map((team) => team.id),
            ],
          },
        ],
      },
    },
    {
      name: [
        '3: Play only against teams outside of own group',
        'Northern Group',
        'Southern Group',
      ],
      teamsEntering: 6,
      rounds: 1,
      stageSettings: {
        scheduleType: ScheduleType.CrossSubLeagues,
        subLeagues: [
          {
            teamIds: [
              ...europeanNationalTeams.map((team) => team.id),
              ...northAmericanNationalTeams.map((team) => team.id),
              ...asianNationalTeams.map((team) => team.id),
            ],
          },
          {
            teamIds: [
              ...southAmericanNationalTeams.map((team) => team.id),
              ...africanNationalTeams.map((team) => team.id),
              ...oceanianNationalTeams.map((team) => team.id),
            ],
          },
        ],
      },
    },
  ],
};

const groupsVsSubLeagues: iCompetitionSettings = {
  id: 100011,
  name: 'Groups vs sub league group',
  stages: [
    {
      name: [
        "1: Groups of which we can't choose its teams",
        'Random Group',
        'Random Group',
      ],
      teamsEntering: 6,
      totalGroups: 2,
      stageSettings: {
        teamIds: [
          ...europeanNationalTeams.slice(0, 1).map((team) => team.id),
          ...northAmericanNationalTeams.slice(0, 1).map((team) => team.id),
          ...southAmericanNationalTeams.slice(0, 1).map((team) => team.id),
          ...asianNationalTeams.slice(0, 1).map((team) => team.id),
          ...africanNationalTeams.slice(0, 1).map((team) => team.id),
          ...oceanianNationalTeams.slice(0, 1).map((team) => team.id),
        ],
      },
    },
    {
      name: [
        '2: Groups with specified teams (using sub leagues)',
        'Northern Group',
        'Southern Group',
      ],
      teamsEntering: 6,
      stageSettings: {
        scheduleType: ScheduleType.InterSubLeagues,
        subLeagues: [
          {
            teamIds: [
              ...europeanNationalTeams.map((team) => team.id),
              ...northAmericanNationalTeams.map((team) => team.id),
              ...asianNationalTeams.map((team) => team.id),
            ],
          },
          {
            teamIds: [
              ...southAmericanNationalTeams.map((team) => team.id),
              ...africanNationalTeams.map((team) => team.id),
              ...oceanianNationalTeams.map((team) => team.id),
            ],
          },
        ],
      },
    },
  ],
};

const advanceEliminateAndPlayoffs: iCompetitionSettings = {
  id: 100005,
  name: 'Advance, eliminate and playoffs',
  stages: [
    {
      name: '1. Group stage',
      rounds: 1,
      stageSettings: {
        teamIds: [...worldNationalTeams.slice(0, 10).map((team) => team.id)],
      },
    },
    {
      name: ['2. Playoff stage', 'Playoff match A.', 'Playoff match B.'],
      rounds: 1,
      teamsSkipping: 3,
      teamsEntering: 4,
      totalGroups: 2,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    {
      name: '3. Group stage (which best team skips)',
      rounds: 1,
      teamsSkipping: 1,
      teamsEntering: 4,
    },
    {
      name: '4. Final',
      rounds: 1,
      teamsEntering: 2,
    },
  ],
};

const pointsCarryingOver: iCompetitionSettings = {
  id: 100006,
  name: 'Points carrying over',
  stages: [
    {
      name: '1. Initial group',
      matchDaysLimit: 1,
      stageSettings: {
        teamIds: [...worldNationalTeams.slice(0, 10).map((team) => team.id)],
      },
    },
    {
      name: '2. Teams recieve all previous points',
      matchDaysLimit: 1,
      teamsEntering: 10,
      previousRoundPointsFactor: 1,
    },
    {
      name: '3. Teams recieve 0.33 x previous points',
      matchDaysLimit: 1,
      teamsEntering: 10,
      previousRoundPointsFactor: 1 / 3,
      pointRoundingStrategy: MathRoundType.RawValue,
    },
    {
      name: '4. Same, with default rounding',
      matchDaysLimit: 1,
      teamsEntering: 10,
      previousRoundPointsFactor: 1 / 3,
      pointRoundingStrategy: MathRoundType.Round,
    },
    {
      name: '5. Rounding on 3 decimals',
      matchDaysLimit: 1,
      teamsEntering: 10,
      previousRoundPointsFactor: 1 / 3,
      pointRoundingStrategy: MathRoundType.Round3Decimals,
    },
    {
      name: '6. Rounding up',
      matchDaysLimit: 1,
      teamsEntering: 10,
      previousRoundPointsFactor: 1 / 3,
      pointRoundingStrategy: MathRoundType.RoundUp,
    },
    {
      name: '7. Rounding down',
      matchDaysLimit: 1,
      teamsEntering: 10,
      previousRoundPointsFactor: 1 / 3,
      pointRoundingStrategy: MathRoundType.RoundDown,
    },
  ],
};

const drawTypes: iCompetitionSettings = {
  id: 100007,
  name: 'Draw types',
  stages: [
    {
      name: '1. Initial group',
      rounds: 1,
      stageSettings: {
        teamIds: [...worldNationalTeams.slice(0, 10).map((team) => team.id)],
      },
    },
    {
      name: ['2. 2 randomly drawn groups', 'Random group 1', 'Random group 2'],
      rounds: 1,
      totalGroups: 2,
      teamsEntering: 10,
    },
    {
      name: [
        '3. Balanced groups based on team strength',
        'Balanced group 1',
        'Balanced group 2',
      ],
      rounds: 1,
      totalGroups: 2,
      teamsEntering: 10,
      drawType: DrawType.Reputation,
    },
    {
      name: [
        '4. Same, but based on previous round performances',
        'Balanced group 1',
        'Balanced group 2',
      ],
      rounds: 1,
      totalGroups: 2,
      teamsEntering: 10,
      drawType: DrawType.Performance,
    },
    {
      name: [
        '5. Strong and weak group (based on performance)',
        'Strong group',
        'Weak group',
      ],
      rounds: 1,
      totalGroups: 2,
      teamsEntering: 10,
      drawType: DrawType.EqualPerformance,
    },
    {
      name: [
        '6. Final, 3th & 5th place play-off',
        'Final',
        '3th Place Play-off',
        '5th Place Play-off',
      ],
      rounds: 1,
      totalGroups: 3,
      teamsEntering: 6,
      drawType: DrawType.EqualPerformance,
    },
  ],
};

export const tutorialCompetitions = [
  groupAndKnockOutStages,
  swissSystem,
  subLeaguesBasics,
  subLeaguesSchedules,
  groupsVsSubLeagues,
  advanceEliminateAndPlayoffs,
  pointsCarryingOver,
  drawTypes,
];

const leagueSystemLeague: iCompetitionSettings = {
  id: 100008,
  name: 'European league',
  stages: [
    {
      name: '(1 of 4 competitions)',
      rounds: 1,
      stageSettings: {
        teamIds: [...europeanNationalTeams.slice(0, 10).map((team) => team.id)],
      },
    },
  ],
};
const leagueSystemCup: iCompetitionSettings = {
  id: 100009,
  name: 'South American Cup',
  stages: [
    {
      name: 'Quarter Final',
      rounds: 1,
      totalGroups: 4,
      tiebrakeBy: MatchFases.ExtraTime,
      stageSettings: {
        teamIds: [
          ...southAmericanNationalTeams.slice(0, 8).map((team) => team.id),
        ],
      },
    },
    {
      name: 'Semi Final',
      rounds: 1,
      totalGroups: 2,
      teamsEntering: 4,
      tiebrakeBy: MatchFases.ExtraTime,
    },
    {
      name: ['Finals', 'Final', 'Third Place Play-off'],
      rounds: 1,
      totalGroups: 2,
      teamsEntering: 4,
      tiebrakeBy: MatchFases.ExtraTime,
      drawType: DrawType.EqualPerformance,
    },
  ],
};
const superCup: iCompetitionSettings = {
  id: 100010,
  name: 'Super Cup',
  stages: [
    {
      name: '(Both champions qualify)',
      rounds: 1,
      tiebrakeBy: MatchFases.ExtraTime,
      stageSettings: {
        teamIds: [...worldNationalTeams.slice(0, 2).map((team) => team.id)],
      },
    },
  ],
};
const top4Cup: iCompetitionSettings = {
  id: 100012,
  name: 'Conference cup',
  stages: [
    {
      name: [
        '(Best ranked teams from both competitions qualify)',
        'Combined standings',
        'European Zone',
        'South American Zone',
      ],
      stageSettings: {
        subLeagues: [
          {
            teamIds: [...europeanNationalTeams.map((team) => team.id)],
          },
          {
            teamIds: [...southAmericanNationalTeams.map((team) => team.id)],
          },
        ],
      },
    },
    {
      name: 'Final',
      rounds: 1,
      teamsEntering: 2,
      tiebrakeBy: MatchFases.ExtraTime,
    },
  ],
};
const tutorialLeagueSystem: iLeagueSystem = {
  id: 1000,
  name: 'Tutorial League System',
  competitions: [
    get(leagueSystemLeague),
    get(leagueSystemCup),
    get(superCup),
    get(top4Cup),
  ],
  includedTeamsFromOtherCompetition: [
    { sourceIndex: 0, targetIndex: 2, teamsEntering: 1 },
    { sourceIndex: 1, targetIndex: 2, teamsEntering: 1 },
    { sourceIndex: 0, targetIndex: 3, teamsEntering: 5 },
    { sourceIndex: 1, targetIndex: 3, teamsEntering: 3 },
  ],
};

export const tutorialLeagueSystems = [tutorialLeagueSystem];

function get(competitionSettings) {
  return JSON.parse(JSON.stringify(competitionSettings));
}
