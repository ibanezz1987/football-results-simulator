import { Continent, iTeam } from '../models';

export const nationalTeams: iTeam[] = [
  { id: 1, name: 'Belgium', continent: Continent.Europe, strength: 100 , isNationalTeam: true },
  { id: 2, name: 'France', continent: Continent.Europe, strength: 97 , isNationalTeam: true },
  { id: 3, name: 'England', continent: Continent.Europe, strength: 87 , isNationalTeam: true },
  { id: 4, name: 'Portugal', continent: Continent.Europe, strength: 86 , isNationalTeam: true },
  { id: 5, name: 'Spain', continent: Continent.Europe, strength: 84 , isNationalTeam: true },
  { id: 6, name: 'Italy', continent: Continent.Europe, strength: 81 , isNationalTeam: true },
  { id: 7, name: 'Croatia', continent: Continent.Europe, strength: 80 , isNationalTeam: true },
  { id: 8, name: 'Denmark', continent: Continent.Europe, strength: 80 , isNationalTeam: true },
  { id: 9, name: 'Germany', continent: Continent.Europe, strength: 79 , isNationalTeam: true },
  { id: 10, name: 'Netherlands', continent: Continent.Europe, strength: 79 , isNationalTeam: true },
  { id: 11, name: 'Switzerland', continent: Continent.Europe, strength: 77 , isNationalTeam: true },
  { id: 12, name: 'Wales', continent: Continent.Europe, strength: 74 , isNationalTeam: true },
  { id: 13, name: 'Poland', continent: Continent.Europe, strength: 73 , isNationalTeam: true },
  { id: 14, name: 'Sweden', continent: Continent.Europe, strength: 73 , isNationalTeam: true },
  { id: 15, name: 'Austria', continent: Continent.Europe, strength: 70 , isNationalTeam: true },
  { id: 16, name: 'Ukraine', continent: Continent.Europe, strength: 68 , isNationalTeam: true },
  { id: 17, name: 'Serbia', continent: Continent.Europe, strength: 65 , isNationalTeam: true },
  { id: 18, name: 'Turkey', continent: Continent.Europe, strength: 64 , isNationalTeam: true },
  { id: 19, name: 'Slovakia', continent: Continent.Europe, strength: 63 , isNationalTeam: true },
  { id: 20, name: 'Romania', continent: Continent.Europe, strength: 62 , isNationalTeam: true },
  { id: 21, name: 'Russia', continent: Continent.Europe, strength: 61 , isNationalTeam: true },
  { id: 22, name: 'Hungary', continent: Continent.Europe, strength: 61 , isNationalTeam: true },
  { id: 23, name: 'Republic of Ireland', continent: Continent.Europe, strength: 60 , isNationalTeam: true },
  { id: 24, name: 'Czech Republic', continent: Continent.Europe, strength: 60 , isNationalTeam: true },
  { id: 25, name: 'Norway', continent: Continent.Europe, strength: 60 , isNationalTeam: true },
  { id: 26, name: 'Northern Ireland', continent: Continent.Europe, strength: 58 , isNationalTeam: true },
  { id: 27, name: 'Iceland', continent: Continent.Europe, strength: 58 , isNationalTeam: true },
  { id: 28, name: 'Scotland', continent: Continent.Europe, strength: 58 , isNationalTeam: true },
  { id: 29, name: 'Greece', continent: Continent.Europe, strength: 55 , isNationalTeam: true },
  { id: 30, name: 'Finland', continent: Continent.Europe, strength: 55 , isNationalTeam: true },
  { id: 31, name: 'Bosnia-Herzegovina', continent: Continent.Europe, strength: 55 , isNationalTeam: true },
  { id: 32, name: 'Slovenia', continent: Continent.Europe, strength: 51 , isNationalTeam: true },
  { id: 33, name: 'Montenegro', continent: Continent.Europe, strength: 50 , isNationalTeam: true },
  { id: 34, name: 'North Macedonia', continent: Continent.Europe, strength: 49 , isNationalTeam: true },
  { id: 35, name: 'Albania', continent: Continent.Europe, strength: 49 , isNationalTeam: true },
  { id: 36, name: 'Bulgaria', continent: Continent.Europe, strength: 47 , isNationalTeam: true },
  { id: 37, name: 'Israel', continent: Continent.Europe, strength: 39 , isNationalTeam: true },
  { id: 38, name: 'Belarus', continent: Continent.Europe, strength: 37 , isNationalTeam: true },
  { id: 39, name: 'Georgia', continent: Continent.Europe, strength: 37 , isNationalTeam: true },
  { id: 40, name: 'Luxembourg', continent: Continent.Europe, strength: 33 , isNationalTeam: true },
  { id: 41, name: 'Armenia', continent: Continent.Europe, strength: 33 , isNationalTeam: true },
  { id: 42, name: 'Cyprus', continent: Continent.Europe, strength: 32 , isNationalTeam: true },
  { id: 43, name: 'Faroe Islands', continent: Continent.Europe, strength: 27 , isNationalTeam: true },
  { id: 44, name: 'Azerbaijan', continent: Continent.Europe, strength: 26 , isNationalTeam: true },
  { id: 45, name: 'Estonia', continent: Continent.Europe, strength: 26 , isNationalTeam: true },
  { id: 46, name: 'Kosovo', continent: Continent.Europe, strength: 23 , isNationalTeam: true },
  { id: 47, name: 'Kazakhstan', continent: Continent.Europe, strength: 22 , isNationalTeam: true },
  { id: 48, name: 'Lithuania', continent: Continent.Europe, strength: 18 , isNationalTeam: true },
  { id: 49, name: 'Latvia', continent: Continent.Europe, strength: 14 , isNationalTeam: true },
  { id: 50, name: 'Andorra', continent: Continent.Europe, strength: 10 , isNationalTeam: true },
  { id: 51, name: 'Malta', continent: Continent.Europe, strength: 1 , isNationalTeam: true },
  { id: 52, name: 'Moldova', continent: Continent.Europe, strength: 1 , isNationalTeam: true },
  { id: 53, name: 'Liechtenstein', continent: Continent.Europe, strength: 1 , isNationalTeam: true },
  { id: 54, name: 'Gibraltar', continent: Continent.Europe, strength: 1 , isNationalTeam: true },
  { id: 55, name: 'San Marino', continent: Continent.Europe, strength: 1 , isNationalTeam: true },
  { id: 56, name: 'Brazil', continent: Continent.SouthAmerica, strength: 96 , isNationalTeam: true },
  { id: 57, name: 'Argentina', continent: Continent.SouthAmerica, strength: 83 , isNationalTeam: true },
  { id: 58, name: 'Uruguay', continent: Continent.SouthAmerica, strength: 83 , isNationalTeam: true },
  { id: 59, name: 'Colombia', continent: Continent.SouthAmerica, strength: 78 , isNationalTeam: true },
  { id: 60, name: 'Chile', continent: Continent.SouthAmerica, strength: 74 , isNationalTeam: true },
  { id: 61, name: 'Peru', continent: Continent.SouthAmerica, strength: 67 , isNationalTeam: true },
  { id: 62, name: 'Venezuela', continent: Continent.SouthAmerica, strength: 66 , isNationalTeam: true },
  { id: 63, name: 'Paraguay', continent: Continent.SouthAmerica, strength: 63 , isNationalTeam: true },
  { id: 64, name: 'Ecuador', continent: Continent.SouthAmerica, strength: 55 , isNationalTeam: true },
  { id: 65, name: 'Bolivia', continent: Continent.SouthAmerica, strength: 42 , isNationalTeam: true },
  { id: 66, name: 'Mexico', continent: Continent.NorthAmerica, strength: 82 , isNationalTeam: true },
  { id: 67, name: 'United States', continent: Continent.NorthAmerica, strength: 72 , isNationalTeam: true },
  { id: 68, name: 'Jamaica', continent: Continent.NorthAmerica, strength: 58 , isNationalTeam: true },
  { id: 69, name: 'Costa Rica', continent: Continent.NorthAmerica, strength: 57 , isNationalTeam: true },
  { id: 70, name: 'Honduras', continent: Continent.NorthAmerica, strength: 49 , isNationalTeam: true },
  { id: 71, name: 'El Salvador', continent: Continent.NorthAmerica, strength: 47 , isNationalTeam: true },
  { id: 72, name: 'Canada', continent: Continent.NorthAmerica, strength: 45 , isNationalTeam: true },
  { id: 73, name: 'Curaçao', continent: Continent.NorthAmerica, strength: 43 , isNationalTeam: true },
  { id: 74, name: 'Panama', continent: Continent.NorthAmerica, strength: 43 , isNationalTeam: true },
  { id: 75, name: 'Haiti', continent: Continent.NorthAmerica, strength: 39 , isNationalTeam: true },
  { id: 76, name: 'Trinidad and Tobago', continent: Continent.NorthAmerica, strength: 29 , isNationalTeam: true },
  { id: 77, name: 'Antigua and Barbuda', continent: Continent.NorthAmerica, strength: 20 , isNationalTeam: true },
  { id: 78, name: 'Guatemala', continent: Continent.NorthAmerica, strength: 18 , isNationalTeam: true },
  { id: 79, name: 'St. Kitts and Nevis', continent: Continent.NorthAmerica, strength: 13 , isNationalTeam: true },
  { id: 80, name: 'Suriname', continent: Continent.NorthAmerica, strength: 13 , isNationalTeam: true },
  { id: 81, name: 'Nicaragua', continent: Continent.NorthAmerica, strength: 11 , isNationalTeam: true },
  { id: 82, name: 'Dominican Republic', continent: Continent.NorthAmerica, strength: 6 , isNationalTeam: true },
  { id: 83, name: 'Grenada', continent: Continent.NorthAmerica, strength: 6 , isNationalTeam: true },
  { id: 84, name: 'Barbados', continent: Continent.NorthAmerica, strength: 5 , isNationalTeam: true },
  { id: 85, name: 'Guyana', continent: Continent.NorthAmerica, strength: 3 , isNationalTeam: true },
  { id: 86, name: 'St. Vincent / Grenadines', continent: Continent.NorthAmerica, strength: 2 , isNationalTeam: true },
  { id: 87, name: 'Bermuda', continent: Continent.NorthAmerica, strength: 2 , isNationalTeam: true },
  { id: 88, name: 'Belize', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 89, name: 'St. Lucia', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 90, name: 'Puerto Rico', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 91, name: 'Cuba', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 92, name: 'Montserrat', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 93, name: 'Dominica', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 94, name: 'Cayman Islands', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 95, name: 'Bahamas', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 96, name: 'Aruba', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 97, name: 'Turks and Caicos Islands', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 98, name: 'US Virgin Islands', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 99, name: 'British Virgin Islands', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 100, name: 'Anguilla', continent: Continent.NorthAmerica, strength: 1 , isNationalTeam: true },
  { id: 101, name: 'Japan', continent: Continent.Asia, strength: 66 , isNationalTeam: true },
  { id: 102, name: 'Iran', continent: Continent.Asia, strength: 65 , isNationalTeam: true },
  { id: 103, name: 'Korea Republic', continent: Continent.Asia, strength: 62 , isNationalTeam: true },
  { id: 104, name: 'Australia', continent: Continent.Asia, strength: 61 , isNationalTeam: true },
  { id: 105, name: 'Qatar', continent: Continent.Asia, strength: 52 , isNationalTeam: true },
  { id: 106, name: 'Saudi Arabia', continent: Continent.Asia, strength: 48 , isNationalTeam: true },
  { id: 107, name: 'Iraq', continent: Continent.Asia, strength: 47 , isNationalTeam: true },
  { id: 108, name: 'UAE', continent: Continent.Asia, strength: 44 , isNationalTeam: true },
  { id: 109, name: 'China PR', continent: Continent.Asia, strength: 44 , isNationalTeam: true },
  { id: 110, name: 'Syria', continent: Continent.Asia, strength: 43 , isNationalTeam: true },
  { id: 111, name: 'Oman', continent: Continent.Asia, strength: 41 , isNationalTeam: true },
  { id: 112, name: 'Uzbekistan', continent: Continent.Asia, strength: 39 , isNationalTeam: true },
  { id: 113, name: 'Lebanon', continent: Continent.Asia, strength: 37 , isNationalTeam: true },
  { id: 114, name: 'Vietnam', continent: Continent.Asia, strength: 36 , isNationalTeam: true },
  { id: 115, name: 'Jordan', continent: Continent.Asia, strength: 34 , isNationalTeam: true },
  { id: 116, name: 'Kyrgyz Republic', continent: Continent.Asia, strength: 34 , isNationalTeam: true },
  { id: 117, name: 'Bahrain', continent: Continent.Asia, strength: 33 , isNationalTeam: true },
  { id: 118, name: 'Palestine', continent: Continent.Asia, strength: 29 , isNationalTeam: true },
  { id: 119, name: 'India', continent: Continent.Asia, strength: 27 , isNationalTeam: true },
  { id: 120, name: 'Thailand', continent: Continent.Asia, strength: 26 , isNationalTeam: true },
  { id: 121, name: 'Korea DPR', continent: Continent.Asia, strength: 25 , isNationalTeam: true },
  { id: 122, name: 'Tajikistan', continent: Continent.Asia, strength: 22 , isNationalTeam: true },
  { id: 123, name: 'Philippines', continent: Continent.Asia, strength: 21 , isNationalTeam: true },
  { id: 124, name: 'Turkmenistan', continent: Continent.Asia, strength: 17 , isNationalTeam: true },
  { id: 125, name: 'Myanmar', continent: Continent.Asia, strength: 14 , isNationalTeam: true },
  { id: 126, name: 'Chinese Taipei', continent: Continent.Asia, strength: 14 , isNationalTeam: true },
  { id: 127, name: 'Hong Kong', continent: Continent.Asia, strength: 13 , isNationalTeam: true },
  { id: 128, name: 'Yemen', continent: Continent.Asia, strength: 13 , isNationalTeam: true },
  { id: 129, name: 'Kuwait', continent: Continent.Asia, strength: 11 , isNationalTeam: true },
  { id: 130, name: 'Afghanistan', continent: Continent.Asia, strength: 10 , isNationalTeam: true },
  { id: 131, name: 'Malaysia', continent: Continent.Asia, strength: 9 , isNationalTeam: true },
  { id: 132, name: 'Maldives', continent: Continent.Asia, strength: 9 , isNationalTeam: true },
  { id: 133, name: 'Singapore', continent: Continent.Asia, strength: 6 , isNationalTeam: true },
  { id: 134, name: 'Nepal', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 135, name: 'Cambodia', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 136, name: 'Indonesia', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 137, name: 'Macao', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 138, name: 'Bangladesh', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 139, name: 'Laos', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 140, name: 'Bhutan', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 141, name: 'Mongolia', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 142, name: 'Brunei', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 143, name: 'Timor-Leste', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 144, name: 'Guam', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 145, name: 'Pakistan', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 146, name: 'Sri Lanka', continent: Continent.Asia, strength: 1 , isNationalTeam: true },
  { id: 147, name: 'Senegal', continent: Continent.Africa, strength: 73 , isNationalTeam: true },
  { id: 148, name: 'Tunisia', continent: Continent.Africa, strength: 66 , isNationalTeam: true },
  { id: 149, name: 'Algeria', continent: Continent.Africa, strength: 64 , isNationalTeam: true },
  { id: 150, name: 'Morocco', continent: Continent.Africa, strength: 64 , isNationalTeam: true },
  { id: 151, name: 'Nigeria', continent: Continent.Africa, strength: 63 , isNationalTeam: true },
  { id: 152, name: 'Egypt', continent: Continent.Africa, strength: 57 , isNationalTeam: true },
  { id: 153, name: 'Cameroon', continent: Continent.Africa, strength: 57 , isNationalTeam: true },
  { id: 154, name: 'Ghana', continent: Continent.Africa, strength: 56 , isNationalTeam: true },
  { id: 155, name: 'Mali', continent: Continent.Africa, strength: 55 , isNationalTeam: true },
  { id: 156, name: 'Burkina Faso', continent: Continent.Africa, strength: 52 , isNationalTeam: true },
  { id: 157, name: 'Congo DR', continent: Continent.Africa, strength: 52 , isNationalTeam: true },
  { id: 158, name: "Côte d'Ivoire", continent: Continent.Africa, strength: 52 , isNationalTeam: true },
  { id: 159, name: 'South Africa', continent: Continent.Africa, strength: 46 , isNationalTeam: true },
  { id: 160, name: 'Guinea', continent: Continent.Africa, strength: 45 , isNationalTeam: true },
  { id: 161, name: 'Cape Verde Islands', continent: Continent.Africa, strength: 42 , isNationalTeam: true },
  { id: 162, name: 'Benin', continent: Continent.Africa, strength: 41 , isNationalTeam: true },
  { id: 163, name: 'Uganda', continent: Continent.Africa, strength: 41 , isNationalTeam: true },
  { id: 164, name: 'Gabon', continent: Continent.Africa, strength: 39 , isNationalTeam: true },
  { id: 165, name: 'Congo', continent: Continent.Africa, strength: 37 , isNationalTeam: true },
  { id: 166, name: 'Zambia', continent: Continent.Africa, strength: 37 , isNationalTeam: true },
  { id: 167, name: 'Madagascar', continent: Continent.Africa, strength: 36 , isNationalTeam: true },
  { id: 168, name: 'Mauritania', continent: Continent.Africa, strength: 30 , isNationalTeam: true },
  { id: 169, name: 'Kenya', continent: Continent.Africa, strength: 27 , isNationalTeam: true },
  { id: 170, name: 'Mozambique', continent: Continent.Africa, strength: 27 , isNationalTeam: true },
  { id: 171, name: 'Libya', continent: Continent.Africa, strength: 26 , isNationalTeam: true },
  { id: 172, name: 'Zimbabwe', continent: Continent.Africa, strength: 26 , isNationalTeam: true },
  { id: 173, name: 'Niger', continent: Continent.Africa, strength: 25 , isNationalTeam: true },
  { id: 174, name: 'Central African Republic', continent: Continent.Africa, strength: 25 , isNationalTeam: true },
  { id: 175, name: 'Sierra Leone', continent: Continent.Africa, strength: 24 , isNationalTeam: true },
  { id: 176, name: 'Guinea-Bissau', continent: Continent.Africa, strength: 22 , isNationalTeam: true },
  { id: 177, name: 'Namibia', continent: Continent.Africa, strength: 22 , isNationalTeam: true },
  { id: 178, name: 'Malawi', continent: Continent.Africa, strength: 21 , isNationalTeam: true },
  { id: 179, name: 'Angola', continent: Continent.Africa, strength: 21 , isNationalTeam: true },
  { id: 180, name: 'Sudan', continent: Continent.Africa, strength: 19 , isNationalTeam: true },
  { id: 181, name: 'Togo', continent: Continent.Africa, strength: 18 , isNationalTeam: true },
  { id: 182, name: 'Comoros', continent: Continent.Africa, strength: 18 , isNationalTeam: true },
  { id: 183, name: 'Rwanda', continent: Continent.Africa, strength: 17 , isNationalTeam: true },
  { id: 184, name: 'Equatorial Guinea', continent: Continent.Africa, strength: 16 , isNationalTeam: true },
  { id: 185, name: 'Tanzania', continent: Continent.Africa, strength: 15 , isNationalTeam: true },
  { id: 186, name: 'Burundi', continent: Continent.Africa, strength: 14 , isNationalTeam: true },
  { id: 187, name: 'Lesotho', continent: Continent.Africa, strength: 13 , isNationalTeam: true },
  { id: 188, name: 'Ethiopia', continent: Continent.Africa, strength: 12 , isNationalTeam: true },
  { id: 189, name: 'Botswana', continent: Continent.Africa, strength: 12 , isNationalTeam: true },
  { id: 190, name: 'Liberia', continent: Continent.Africa, strength: 10 , isNationalTeam: true },
  { id: 191, name: 'Eswatini', continent: Continent.Africa, strength: 9 , isNationalTeam: true },
  { id: 192, name: 'Gambia', continent: Continent.Africa, strength: 8 , isNationalTeam: true },
  { id: 193, name: 'South Sudan', continent: Continent.Africa, strength: 4 , isNationalTeam: true },
  { id: 194, name: 'Mauritius', continent: Continent.Africa, strength: 1 , isNationalTeam: true },
  { id: 195, name: 'Chad', continent: Continent.Africa, strength: 1 , isNationalTeam: true },
  { id: 196, name: 'Djibouti', continent: Continent.Africa, strength: 1 , isNationalTeam: true },
  { id: 197, name: 'São Tomé e Príncipe', continent: Continent.Africa, strength: 1 , isNationalTeam: true },
  { id: 198, name: 'Somalia', continent: Continent.Africa, strength: 1 , isNationalTeam: true },
  { id: 199, name: 'Seychelles', continent: Continent.Africa, strength: 1 , isNationalTeam: true },
  { id: 200, name: 'Eritrea', continent: Continent.Africa, strength: 1 , isNationalTeam: true },
  { id: 201, name: 'New Zealand', continent: Continent.Oceania, strength: 22 , isNationalTeam: true },
  { id: 202, name: 'Solomon Islands', continent: Continent.Oceania, strength: 13 , isNationalTeam: true },
  { id: 203, name: 'New Caledonia', continent: Continent.Oceania, strength: 8 , isNationalTeam: true },
  { id: 204, name: 'Tahiti', continent: Continent.Oceania, strength: 6 , isNationalTeam: true },
  { id: 205, name: 'Vanuatu', continent: Continent.Oceania, strength: 4 , isNationalTeam: true },
  { id: 206, name: 'Fiji', continent: Continent.Oceania, strength: 4 , isNationalTeam: true },
  { id: 207, name: 'Papua New Guinea', continent: Continent.Oceania, strength: 3 , isNationalTeam: true },
  { id: 208, name: 'American Samoa', continent: Continent.Oceania, strength: 1 , isNationalTeam: true },
  { id: 209, name: 'Samoa', continent: Continent.Oceania, strength: 1 , isNationalTeam: true },
  { id: 210, name: 'Tonga', continent: Continent.Oceania, strength: 1 , isNationalTeam: true },
];
